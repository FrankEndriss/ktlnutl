package com.happypeople.codeforces

import org.junit.Assert.assertEquals
import org.junit.Test

class SegmentTreeTest {

    fun setUp(): SegmentTree<Int> {
        val a = (1..10).toList()
        return SegmentTree(a, 0) { e1, e2 -> e1 + e2 }
    }

    @Test
    fun update1() {
        val tree = setUp()
        tree.update(0, 2)   // added 1
        val upd=1
        assertEquals("first", 1+upd, tree.get(0, 1))
        assertEquals("second", 2, tree.get(1, 2))
        assertEquals("first+second", 3+upd, tree.get(0, 2))
        assertEquals("third", 3, tree.get(2, 3))

        assertEquals("a[7]", 8, tree.get(7, 8))
        assertEquals("a[8]", 9, tree.get(8, 9))
        assertEquals("a[9]", 10, tree.get(9, 10))

        assertEquals("a[7]+a[8]", 17, tree.get(7, 9))
        assertEquals("8+9+10==27", 27, tree.get(7, 10))
    }

    @Test
    fun update2() {
        val tree = setUp()
        tree.update(4, 10)   // added 5
        val upd=5
        assertEquals("first", 1, tree.get(0, 1))
        assertEquals("second", 2, tree.get(1, 2))
        assertEquals("first+second", 3, tree.get(0, 2))
        assertEquals("third", 3, tree.get(2, 3))

        assertEquals("a[7]", 8, tree.get(7, 8))
        assertEquals("a[8]", 9, tree.get(8, 9))
        assertEquals("a[9]", 10, tree.get(9, 10))

        assertEquals("a[7]+a[8]", 17, tree.get(7, 9))
        assertEquals("8+9+10==27", 27, tree.get(7, 10))
    }


    @Test
    fun get() {
        val tree = setUp()
        assertEquals("first", 1, tree.get(0, 1))
        assertEquals("second", 2, tree.get(1, 2))
        assertEquals("first+second", 3, tree.get(0, 2))
        assertEquals("third", 3, tree.get(2, 3))

        assertEquals("a[7]", 8, tree.get(7, 8))
        assertEquals("a[8]", 9, tree.get(8, 9))
        assertEquals("a[9]", 10, tree.get(9, 10))

        assertEquals("a[7]+a[8]", 17, tree.get(7, 9))
        assertEquals("8+9+10==27", 27, tree.get(7, 10))
    }
}