package com.happypeople.codeforces

import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Test

class InvTest {
    @Test
    fun t_7_8() {
        val value=7L * Inv.inv(8) % Inv.defaultMod
        assertEquals("first example of https://codeforces.com/contest/1096/problem/E",124780545L, value )
    }

    @Test
    fun testSimpleInf() {
        val  value1=Inv.simpleInf(14, 16)
        val  value2=Inv.simpleInf(7, 8)
        assertEquals("same", value1, value2)
    }
}