package com.happypeople.codeforces

import org.junit.Assert.*
import org.junit.Test

class PermutationsTest {
    @Test
    fun test1() {
        val list=listOf('1', '2', '3', '4')
        val s=Permutations.permutations(list)
        assertEquals("same first", s.first(), list)
        val s2=Permutations.permutations(list)
        assertEquals("reversed last", s2.last(), list.reversed())
    }

    @Test
    fun test2() {
        val s=Permutations.permutations('a'..'c')
        assertEquals("chars","[[a, b, c], [a, c, b], [b, a, c], [b, c, a], [c, a, b], [c, b, a]]", ""+s.toList() )
    }
}