package com.happypeople.ktlnutl

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class PrimesTest {

    @Before
    fun setUp() {
    }

    @Test
    fun asSeq() {
        val sz=50000
        val primes=Primes(sz)
        // val seq=primes.asSeq()
        assertEquals("should be $sz", sz, primes.asSeq().count())
        println(primes.asSeq().drop(sz/2).take(10).toList())
        println(primes.asSeq().drop(sz-5).take(10).toList())
        println(primes.asSeq().take(20).toList())
    }
}