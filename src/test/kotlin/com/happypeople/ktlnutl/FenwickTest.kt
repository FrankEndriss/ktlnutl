package com.happypeople.codeforces

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class FenwickTest {

    @Before
    fun setUp() {
    }

    @Test
    fun test_lsb() {
        assertEquals("110", 2, Lsb.lsb(6))
        assertEquals("0", 0, Lsb.lsb(0))
        assertEquals("1", 1, Lsb.lsb(1))
        assertEquals("10", 2, Lsb.lsb(2))
        assertEquals("11", 1, Lsb.lsb(3))
        assertEquals("100", 4, Lsb.lsb(4))
        assertEquals("101", 1, Lsb.lsb(5))
    }

    @Test
    fun testFenwickSparse() {
        val tree=FenwickSparse(15, 0) { i1, i2 -> i1+i2 }
        testTree(tree)
    }

    @Test
    fun testFenwick() {
        val tree=Fenwick(15, 0) { i1, i2 -> i1+i2 }
        testTree(tree)
    }

    fun testTree(tree:FenwickIf<Int>) {

        tree.updateCumul(4, 4)
        tree.updateCumul(7, 3)

        assertEquals("sum [0, 3]", 0, tree.readCumul(3))
        assertEquals("sum [0, 4]", 4, tree.readCumul(4))
        assertEquals("sum [0, 5]", 4, tree.readCumul(5))
        assertEquals("sum [0, 7]", 7, tree.readCumul(7))
        assertEquals("sum [0, 13]", 7, tree.readCumul(13))

        tree.updateCumul(1, 1)
        assertEquals("sum [0, 7]", 8, tree.readCumul(7))
    }

    @Test
    fun test2D() {
        val tree=Fenwick2D(100000, 100000, 0, true) { v1, v2-> v1+v2 }
        // add some points
        tree.updateCumul(111, 111, 1)
        tree.updateCumul(222, 111, 1)
        tree.updateCumul(111, 222, 1)
        tree.updateCumul(155, 155, 1)

        // count points in rect (0,0):(x,y)
        assertEquals("points in rect 1", 0, tree.readCumul(100, 100))
        assertEquals("points in rect 2", 1, tree.readCumul(112, 112))
        assertEquals("points in rect 3", 0, tree.readCumul(112, 100))
        assertEquals("points in rect 4", 1, tree.readCumul(112, 160))
        assertEquals("points in rect 5", 2, tree.readCumul(160, 160))
        assertEquals("points in rect 6", 3, tree.readCumul(250, 200))
        assertEquals("points in rect 7", 4, tree.readCumul(250, 250))
    }
}