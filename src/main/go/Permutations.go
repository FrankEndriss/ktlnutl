
type Ptype int64
func permutations(arr []Ptype, cb func([]Ptype)) {
	var helper func([]Ptype, int)

	helper = func(arr []Ptype, n int) {
		if n == 1 {
			cb(arr)
		} else {
			for i := 0; i < n; i++ {
				helper(arr, n-1)
				if n%2 == 1 {
					tmp := arr[i]
					arr[i] = arr[n-1]
					arr[n-1] = tmp
				} else {
					tmp := arr[0]
					arr[0] = arr[n-1]
					arr[n-1] = tmp
				}
			}
		}
	}
	helper(arr, len(arr))
}
