
// DSU implementation inspired by https://atcoder.github.io/ac-library/production/document_en/dsu.html
type Dsu struct {
	_n             ll
	parent_or_size []ll
}

func NewDsu(n ll) Dsu {
	dsu := Dsu{
		n,
		make([]ll, n)}
	for i := _0; i < n; i++ {
		dsu.parent_or_size[i] = -1
	}
	return dsu
}

func (d Dsu) Leader(a ll) ll {
	if d.parent_or_size[a] < _0 {
		return a
	}
	d.parent_or_size[a] = d.Leader(d.parent_or_size[a])
	return d.parent_or_size[a]
}

func (d Dsu) Size(a ll) ll {
	return -d.parent_or_size[d.Leader(a)]
}

func (d Dsu) Same(a ll, b ll) bool {
	return d.Leader(a) == d.Leader(b)
}

func (d Dsu) Merge(a ll, b ll) ll {
	x, y := d.Leader(a), d.Leader(b)
	if x == y {
		return x
	}
	if -d.parent_or_size[x] < -d.parent_or_size[y] {
		x, y = y, x
	}

	d.parent_or_size[x] += d.parent_or_size[y]
	d.parent_or_size[y] = x
	return x
}

func (d Dsu) Groups() [][]ll {
	leader_buf := make([]ll, d._n)
	group_size := make([]ll, d._n)

	for i := _0; i < d._n; i++ {
		leader_buf[i] = d.Leader(i)
		group_size[leader_buf[i]]++
	}

	res1 := make([][]ll, d._n)
	for i := _0; i < d._n; i++ {
		res1[i] = make([]ll, 0, group_size[i])
	}
	for i := _0; i < d._n; i++ {
		res1[leader_buf[i]] = append(res1[leader_buf[i]], i)
	}

	res := [][]ll{}
	for i := _0; i < d._n; i++ {
		if len(res1[i]) > 0 {
			res = append(res, res1[i])
		}
	}

	return res
}
