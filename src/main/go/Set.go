
type SetT int64
type Set map[SetT]bool

func (s Set) insert(n SetT) {
	s[n] = true
}

func (s Set) sorted()[]SetT {
	ans:=make([]SetT,0,len(s))
	for v:=range s {
		ans=append(ans, v)
	}
	sort.Slice(ans, func(i,j int)bool { return ans[i]<ans[j]})
	return ans;
}

