
/* Usually we can set this to 'z'+1
 * Or consider to update s[i]=s[i]-'a'
 * and set it to 'z'-'a'+1 */
const alphabet = 256

/* construct the suffix array,
 * see https://cp-algorithms.com/string/suffix-array.html
 */
func sort_cyclic_shifts(s []byte) ([][]int, []int) {
	cc_g := make([][]int, 0)

	n := len(s)

	p := make([]int, n)
	c := make([]int, n)
	cnt := make([]int, max(alphabet, int64(n)))

	for i := 0; i < n; i++ {
		cnt[s[i]]++
	}
	for i := 1; i < alphabet; i++ {
		cnt[i] += cnt[i-1]
	}
	for i := 0; i < n; i++ {
		cnt[s[i]]--
		p[cnt[s[i]]] = i
	}

	c[p[0]] = 0
	classes := 1
	for i := 1; i < n; i++ {
		if s[p[i]] != s[p[i-1]] {
			classes++
		}
		c[p[i]] = classes - 1
	}

	pn := make([]int, n)
	cn := make([]int, n)
	for h := 0; (1 << h) < n; h++ {
		for i := 0; i < n; i++ {
			pn[i] = p[i] - (1 << h)
			if pn[i] < 0 {
				pn[i] += n
			}
		}
		for i := 0; i < classes; i++ {
			cnt[i] = 0
		}

		for i := 0; i < n; i++ {
			cnt[c[pn[i]]]++
		}
		for i := 1; i < classes; i++ {
			cnt[i] += cnt[i-1]
		}
		for i := n - 1; i >= 0; i-- {
			cnt[c[pn[i]]]--
			p[cnt[c[pn[i]]]] = pn[i]
		}
		cn[p[0]] = 0
		classes = 1
		for i := 1; i < n; i++ {
			cur := []int{c[p[i]], c[(p[i]+(1<<h))%n]}
			prev := []int{c[p[i-1]], c[(p[i-1]+(1<<h))%n]}
			if cur[0] != prev[0] || cur[1] != prev[1] {
				classes++
			}
			cn[p[i]] = classes - 1
		}
		aux := make([]int, len(c))
		copy(aux, c)
		cc_g = append(cc_g, aux)
		c, cn = cn, c
	}
	return cc_g, p
}

// see https://cp-algorithms.com/string/suffix-array.html#toc-tgt-8
func solve() {
	s := cinb()
	cc_g, _ := sort_cyclic_shifts(s)

	q := cini()
	for i := _0; i < q; i++ {
		a := cini()
		b := cini()
		ans := _0
		for k := len(cc_g) - 1; k >= 0; k-- {
			if cc_g[k][a] == cc_g[k][b] {
				ans += (_1 << k)
				a += _1 << k
				b += _1 << k
			}
		}
		Println(ans)
	}
}

func main() {
	r = bufio.NewReader(os.Stdin)
	wr = bufio.NewWriter(os.Stdout)
	er = bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	//for t:=cini(); t>0; t-- {
	solve()
	//}
}
