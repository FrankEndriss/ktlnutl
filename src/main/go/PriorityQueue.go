
// PriorityQueue<int>, see https://pkg.go.dev/container/heap
// The constructor supports a configuration by a Less function.
// We are supposed to store the actual data in some slice, and put indexes
// of that slice into the PriorityQueue
// use like:
// heap.Push(&q, 42)
// val:=heap.Pop(&q)
// Pop returns the smallest value first according to Less.
type PriorityQueueInt struct {
	data []int64
	less func(i, j int64) bool
}

func NewPriorityQueueInt(less func(i, j int64) bool) PriorityQueueInt {
	return PriorityQueueInt{
		make([]int64, 0),
		less}
}

func (q PriorityQueueInt) Len() int           { return len(q.data) }
func (q PriorityQueueInt) Less(i, j int) bool { return q.less(q.data[i], q.data[j]) }
func (q *PriorityQueueInt) Swap(i, j int)     { q.data[i], q.data[j] = q.data[j], q.data[i] }

func (q *PriorityQueueInt) Push(i interface{}) { q.data = append(q.data, i.(int64)) }
func (q *PriorityQueueInt) Pop() interface{} {
	ans := q.data[len(q.data)-1]
	q.data = q.data[0 : len(q.data)-1]
	return ans
}
