
/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
struct fenwick {
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void update(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    T query(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

template <typename T>
struct fenwick2d {
    vector<fenwick<T>> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick<int>(m));
    }

    void update(int x, int y, T v) {
        while (x < n) {
            fenw2d[x].update(y, v);
            x |= (x + 1);
        }
    }

    T query(int x, int y) {   // range 0..x/y, including x/y
        x=min(x, n-1);
        y=min(y, m-1);
        T v{};
        while (x >= 0) {
            v += fenw2d[x].query(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

