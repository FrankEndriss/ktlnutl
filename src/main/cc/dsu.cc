/* see https://cp-algorithms.com/data_structures/disjoint_set_union.html
 * Also known as union find structure. 
 * Note that the above link has some optimizations for faster combining of sets if needed. 
 */

struct Dsu {
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

