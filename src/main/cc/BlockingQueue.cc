

const size_t default_qsize=1e9;

/* Template for a multithread blocking queue.
 * The call to pop(item&) blocks until
 * an element is available.
 */
template<typename T>
class bqueue {
    std::deque<T> content;
    size_t capacity;

    std::mutex mutex;
    std::condition_variable not_empty;
    std::condition_variable not_full;

    bqueue(const bqueue &) = delete;
    bqueue(bqueue &&) = delete;
    bqueue &operator = (const bqueue &) = delete;
    bqueue &operator = (bqueue &&) = delete;

  public:
    bqueue(size_t capacity=detault_qsize): capacity(capacity) {}

    void push(T &&item) {
        {
            std::unique_lock<std::mutex> lk(mutex);
            not_full.wait(lk, [this]() {
                return content.size() < capacity;
            });
            content.push_back(std::move(item));
        }
        not_empty.notify_one();
    }

    bool try_push(T &&item) {
        {
            std::unique_lock<std::mutex> lk(mutex);
            if (content.size() == capacity)
                return false;
            content.push_back(std::move(item));
        }
        not_empty.notify_one();
        return true;
    }

    void pop(T &item) {
        {
            std::unique_lock<std::mutex> lk(mutex);
            not_empty.wait(lk, [this]() {
                return !content.empty();
            });
            item = std::move(content.front());
            content.pop_front();
        }
        not_full.notify_one();
    }

    bool try_pop(T &item) {
        {
            std::unique_lock<std::mutex> lk(mutex);
            if (content.empty())
                return false;
            item = std::move(content.front());
            content.pop_front();
        }
        not_full.notify_one();
        return true;
    }
};
