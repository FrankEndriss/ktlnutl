
/* Utilize Manacher's algo.
 * see https://cp-algorithms.com/string/manacher.html
 * @return d1[i]= size of left half of palindrome with center at i, ie if size==5 then d1[i]==3
 **/
vector<int> manacher_d1(string s) {
    const int n=s.size();
    vi d1(n);
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 1 : min(d1[l + r - i], r - i + 1);
        while (0 <= i - k && i + k < n && s[i - k] == s[i + k]) {
            k++;
        }
        d1[i] = k--;
        if (i + k > r) {
            l = i - k;
            r = i + k;
        }
    }
    return d1;
}

/*
 * @return d2[i]= size of half of palindrome with right center at i, ie if size==4 then d2[i]==2
 * and palindrome starts at i-d2[i]
 */
vector<int> manacher_d2(string s) {
    const int n=s.size();
    vi d2(n);
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 0 : min(d2[l + r - i + 1], r - i + 1);
        while (0 <= i - k - 1 && i + k < n && s[i - k - 1] == s[i + k]) {
            k++;
        }
        d2[i] = k--;
        if (i + k > r) {
            l = i - k - 1;
            r = i + k ;
        }
    }
    return d2;
}
