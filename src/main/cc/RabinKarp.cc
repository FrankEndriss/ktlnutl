#include <atcoder/modint>
using namespace atcoder;

using mint=modint1000000007;

/** see https://cp-algorithms.com/string/rabin-karp.html
 * Good mod values are: 31, 53
 **/
struct RabinKarp {
    const mint m;
    const size_t wsize;
    mint hash;
    queue<signed> q;

    const mint mpw;

    /* mm=modulo, wsize=window size.  */
    RabinKarp(size_t pwsize, signed mm=31):m(mint(mm)),wsize(pwsize),mpw(mint(mm).pow(pwsize-1)) {
        assert(wsize>0);
    }

    /** Add the next symbol to the window and return the new hash value.  */
    signed add(signed sym) {
        if(q.size()==wsize) {
            hash-=q.front()*mpw;
            q.pop();
        }
        q.push(sym);

        hash*=m;
        hash+=sym;
        return hash.val();
    }
};

