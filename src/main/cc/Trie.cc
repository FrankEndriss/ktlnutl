

/* Simple trie implementation.
 * Use like 'trienode root()';
 */
struct trienode {
    bool fini=false;
    trienode *chl[26]={0};

    trienode() {
    }

    /* add a string to the trie */
    void add(string &s, const int idx) {
        assert(idx<=(int)s.size());
        if(idx==(int)s.size()) {
            fini=true;
            return;
        }

        if(chl[s[idx]-'a']==0)
            chl[s[idx]-'a']=new trienode();

        chl[s[idx]-'a']->add(s,idx+1);
    };

    /* @return the length of the shortest match found, or -1 */
    int query(string &s, const int idx) {
        if(fini)
            return idx;
        else if(idx<(int)s.size() && chl[s[idx]-'a']!=0)
            return chl[s[idx]-'a']->query(s, idx+1);
        else
            return -1;
    }
};
