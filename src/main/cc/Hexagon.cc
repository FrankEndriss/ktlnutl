
#define E 0
#define SE 1
#define SW 2
#define W 3
#define NW 4
#define NE 5

/* axial coordinates x, y
 * see https://www.redblobgames.com/grids/hexagons/
 * */
vvi offs= {
    {  0,  1 },
    {  1,  0 },
    {  1, -1 },
    {  0, -1 },
    { -1,  0 },
    { -1,  1 }
};

/* cube coordinate offsets */
vvi offsCube={
    { 1, 0, -1 },
    { 0, 1, -1 },
    { -1, 1, 0 },
    { -1, 0, 1 },
    { 0, -1, 1 },
    { 1, -1, 0 }
};

