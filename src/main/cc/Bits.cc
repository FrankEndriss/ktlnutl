
/* Calls action for all submasks!=0 of m, in reverse order. Submask 0 is not called.
 * https://cp-algorithms.com/algebra/all-submasks.html
 **/
void iterateSubmask(ll m, function<void(ll)> action)  {
    for (ll s=m; s; s=(s-1)&m)
        action(s);
}

void some_func() {
// examples of gcc buildins
 
int x = 5328; // 00000000000000000001010011010000

//count leading zeros
auto c= __builtin_clz(x); // 19

//count trailing zeros
c= __builtin_ctz(x); // 4

// count ones
c= __builtin_popcount(x); // 5

// odd/even number of ones
c= __builtin_parity(x); // 1

}

#pragma GCC target("popcnt")

constexpr bool has_single_bit(T x): finds if x is a positive integer 2p, for some integer p≥0.
constexpr T bit_ceil(T x): finds the smallest integer 2p≥x for some integer p≥0.
constexpr T bit_floor(T x): finds the largest integer 2p≤x for some integer p≥0.
constexpr T bit_width(T x): finds the smallest number of bits needed to represent x.
constexpr T rotl(T x, int s): finds the result of bitwise cyclic left-rotation of x by s positions.
constexpr T rotr(T x, int s): finds the result of bitwise cyclic right-rotation of x by s positions.
constexpr int countl_zero(T x): finds the number of consecutive 0 bits in x, starting from the most significant bit.
constexpr int countl_one(T x): finds the number of consecutive 1 bits in x, starting from the most significant bit.
constexpr int countr_zero(T x): finds the number of consecutive 0 bits in x, starting from the least significant bit.
constexpr int countr_one(T x): finds the number of consecutive 1 bits in x, starting from the least significant bit.
constexpr int popcount(T x): finds the number of 1 bits in x.
