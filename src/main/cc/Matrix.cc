
const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* vector by matrix multiplication.
 * https://mathinsight.org/matrix_vector_multiplication
 **/
vi mulVM(vvi &m, vi &v, int mod=MOD) {
    assert(m.size()==m[0].size() && v.size()==m.size());
    vi ans(v.size());
    const int sz=v.size();
    for(int i=0; i<sz; i++)
        for(int j=0; j<sz; j++) 
            ans[i]=pl(ans[i], mul(m[i][j],v[i]));
    return ans;
}

/* matrix multiplication */
vvi mulM(vvi &m1, vvi &m2, int mod=MOD) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]=pl(ans[i][j], mul(m1[i][k], m2[k][j]));
            }
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi a, int p, int mod=MOD) {
    const int sz=a.size();
    vvi res = vvi(sz, vi(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res, mod);
        p >>= 1;
        a = mulM(a, a, mod);
    }
    return res;
}

