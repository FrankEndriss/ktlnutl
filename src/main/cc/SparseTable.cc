
/* Range queries on big datasets in O(1) for idempotent 
 * functions like min/max.
 *
 * https://cp-algorithms.com/data_structures/sparse-table.html
 * Range queries, min/max range queries.
 * SparseTable is an immutable structure, like readonly, dont touch.
 */
const int N=2e5+7;
vi logT(N);
const bool initLog=[]() {
    logT[1]=0;
    for(int i=2; i<N; i++)
        logT[i]=logT[i/2]+1;
    return true;
}();

template<typename E>
struct SparseTable {
    int K;
    vector<vector<E>> table;
    function<E(E, E)> cum;

    template<typename Iterator>
    SparseTable(Iterator beg, Iterator end, function<E(E, E)> cummul):cum(cummul) {
        const int n=distance(beg,end);
        K=logT[n];
        table=vector<vector<E>>(n, vector<E>(K+1));

        int idx=0;
        for(; beg!=end; beg++,idx++)
           table[idx][0] = *beg;

        for (int j = 1; j <= K; j++)
            for (int i = 0; i + (1 << j) <= n; i++)
                table[i][j] = cum(table[i][j-1], table[i + (1 << (j - 1))][j - 1]);
    }

    /* range query for cummul funtions like sum or mult. O(nlogN) */
    E rangeSum(int L, int R) {
        E sum = 0;
        for(int j = K; j >= 0; j--) {
            if ((1 << j) <= R - L + 1) {
                sum = cum(sum, table[L][j]);
                L += 1 << j;
            }
        }
        return sum;
    }

    /* range query for cummul functions like min/max. O(1) 
     * [L,R], ie both including
     **/
    E rangeMinmax(int L, int R) {
        int j = logT[R - L + 1];
        E ans=cum(table[L][j], table[R - (1 << j) + 1][j]);
        return ans;
    }
};
