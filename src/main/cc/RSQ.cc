
/* The concept of prefix sums used to query range sums in O(1) */
template<typename E>
E rsq_add(E e1, E e2) {
    return e1+e2;
}

template<typename E>
E rsq_neg(E e) {
    return -e;
}

template<typename E>
struct RSQ {
    vector<E> p;
    function<E(E,E)> cummul;
    function<E(E)> neg;

    template<typename Iterator>
    RSQ(Iterator beg, Iterator end, function<E(E,E)> pCummul=rsq_add<E>, function<E(E)> pNeg=rsq_neg<E>)
        : cummul(pCummul), neg(pNeg) {
        p.push_back(*beg);
        beg++;
        while(beg!=end) {
            p.push_back(pCummul(p.back(), *beg));
            beg++;
        }
    }

    /* inclusive l, exclusive r */
    E get(int l, int r) {
        r=min((int)p.size(), r);
        l=max(0, l);
        if(r<=l)
            return cummul(p[0], neg(p[0])); // neutral

        E ans=p[r-1];
        if(l>0)
            ans=cummul(ans, neg(p[l-1]));
        return ans;
    }
};
