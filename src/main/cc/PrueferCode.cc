
/* Create pruefer code from adj matrix of tree
 * see https://cp-algorithms.com/graph/pruefer_code.html
 */
vector<int> pruefer_code(vector<vector<int>>& adj) {
    int n = adj.size();
    vector<int> parent(n);
    parent[n-1] = -1;

    function<void(int)> dfs=[&](int v) {
        for (int u : adj[v]) {
            if (u != parent[v]) {
                parent[u] = v;
                dfs(u);
            }
        }
    }

    dfs(n-1);

    int ptr = -1;
    vector<int> degree(n);
    for (int i = 0; i < n; i++) {
        degree[i] = adj[i].size();
        if (degree[i] == 1 && ptr == -1)
            ptr = i;
    }

    vector<int> code(n - 2);
    int leaf = ptr;
    for (int i = 0; i < n - 2; i++) {
        int next = parent[leaf];
        code[i] = next;
        if (--degree[next] == 1 && next < ptr) {
            leaf = next;
        } else {
            ptr++;
            while (degree[ptr] != 1)
                ptr++;
            leaf = ptr;
        }
    }

    return code;
}

/* Create tree from pruefer code.
 * see https://cp-algorithms.com/graph/pruefer_code.html */
vector<pair<int, int>> pruefer_decode(vector<int> const& code) {
    int n = code.size() + 2;
    vector<int> degree(n, 1);
    for (int i : code)
        degree[i]++;

    int ptr = 0;
    while (degree[ptr] != 1)
        ptr++;
    int leaf = ptr;

    vector<pair<int, int>> edges;
    for (int v : code) {
        edges.emplace_back(leaf, v);
        if (--degree[v] == 1 && v < ptr) {
            leaf = v;
        } else {
            ptr++;
            while (degree[ptr] != 1)
                ptr++;
            leaf = ptr;
        }
    }
    edges.emplace_back(leaf, n-1);
    return edges;
}
