
/* Longest common subseq */

// length of lcs
int lcslen = 0;

// dp matrix to store result of sub calls for lcs
vector<vector<int>> dp;

// A memoization based function that returns LCS of
// str1[i..len1-1] and str2[j..len2-1]
template<typename T>
int lcs(vector<T> &str1, vector<T> &str2, int i, int j) {
    int &ret = dp[i][j];

    // base condition
    if (i == str1.size() || j == str2.size())
        return ret = 0;

    // if lcs has been computed
    if (ret != -1)
        return ret;

    ret = 0;

    // if characters are same return previous + 1 else
    // max of two sequences after removing i'th and j'th
    // char one by one
    if (str1[i] == str2[j])
        ret = 1 + lcs(str1, str2, i + 1, j + 1);
    else
        ret = max(lcs(str1, str2, i + 1, j), lcs(str1, str2, i, j + 1));

    return ret;
}

set<int> token;

// Function to print all common sub-sequences of
// length lcslen
template<typename T>
bool printAll(vector<T> &str1, vector<T> &str2, vector<T> &data, int indx1, int indx2, int currlcs, set<T> &token, function<bool()> cb) {
    const int len1 = str1.size();
    const int len2 = str2.size();
    // if currlcs is equal to lcslen then print it
    if (currlcs == lcslen) {
        return cb();
    }

    // if we are done with all the characters of both string
    if (indx1 == len1 || indx2 == len2)
        return true;

    // here we have to print all sub-sequences lexicographically,
    // that's why we start from 'a'to'z' if this character is
    // present in both of them then append it in data[] and same
    // remaining part
    for (T ch : token) {
        // done is a flag to tell that we have printed all the
        // subsequences corresponding to current character
        bool done = false;

        for (int i = indx1; i < len1; i++) {
            // if character ch is present in str1 then check if
            // it is present in str2
            if (ch == str1[i]) {
                for (int j = indx2; j < len2; j++) {
                    // if ch is present in both of them and
                    // remaining length is equal to remaining
                    // lcs length then add ch in sub-sequenece
                    if (ch == str2[j] && lcs(str1, str2, i, j) == lcslen - currlcs) {
                        data[currlcs] = ch;
                        if (!printAll(str1, str2, data, i + 1, j + 1, currlcs + 1, token, cb))
                            return false;
                        done = true;
                        break;
                    }
                }
            }

            // If we found LCS beginning with current character.
            if (done)
                break;
        }
    }
    return true;
}

// LCS Longest Common Subsequence
// This function prints all LCS of str1 and str2
// in lexicographic order.
template<typename T>
void printAllLCSSorted(vector<T> str1, vector<T> str2, function<bool(vector<T>&)> cb) {
    // Find lengths of both strings
    int len1 = str1.size(), len2 = str2.size();

    set<T> token;
    for (T t : str1)
        token.insert(t);
    for (T t : str2)
        token.insert(t);

#ifdef DEBUG
    cout << "len1=" << len1 << " len2=" << len2 << endl;
#endif
    // Find length of LCS
    dp = vector<vector<int>>(len1 + 1, vector<int>(len2 + 1, -1));
    lcslen = lcs(str1, str2, 0, 0);
#ifdef DEBUG
    cout << "lcslen=" << lcslen << endl;
#endif

    // Print all LCS using recursive backtracking
    // data[] is used to store individual LCS.
    vector<int> data(str1.size() + str2.size());
    printAll(str1, str2, data, 0, 0, 0, token, [&]() {
        if (cb(data))
            return true;
        else
            return false;
    });
}
