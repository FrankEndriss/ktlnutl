mod x {

    use std::io::BufRead;
    /** Scanner stolen from  https://codeforces.com/contest/1829/submission/205265254 */
    use std::{io, str};

    pub struct Scanner<R> {
        reader: R,
        buf_str: Vec<u8>,
        buf_iter: str::SplitAsciiWhitespace<'static>,
    }
    impl<R: io::BufRead> Scanner<R> {
        pub fn new(reader: R) -> Self {
            Self {
                reader,
                buf_str: vec![],
                buf_iter: "".split_ascii_whitespace(),
            }
        }
        pub fn next<T: str::FromStr>(&mut self) -> T {
            loop {
                if let Some(token) = self.buf_iter.next() {
                    return token.parse().ok().expect("Failed parse");
                }
                self.buf_str.clear();
                self.reader
                    .read_until(b'\n', &mut self.buf_str)
                    .expect("Failed read");
                self.buf_iter = unsafe {
                    let slice = str::from_utf8_unchecked(&self.buf_str);
                    std::mem::transmute(slice.split_ascii_whitespace())
                }
            }
        }
    }

    /*
    use std::cmp::min;
    use std::cmp::max;
    */

    /** 
     */
    pub fn solve<R>(mut inp: Scanner<R>)
    where
        R: BufRead,
    {
    }
}

use std::io;

fn main() {
    x::solve(x::Scanner::new(io::stdin().lock()));
}

#[cfg(test)]
mod test {
    use crate::x;
    use std::io::{BufReader, Cursor};

    #[test]
    fn my_test() {
        let input = "
            2 2
            ";
        x::solve(x::Scanner::new(BufReader::new(Cursor::new(input))));
    }
}
