
#include <ext/rope>
using namespace __gnu_cxx;

/***************
use like
    // https://en.wikipedia.org/wiki/Rope_(data_structure)
    rope<int> r;
    r.insert(r.mutable_begin(), 42);
    r.insert(r.mutable_begin()+1, 43);
    r.insert(r.mutable_begin(), 50);
    for(int i=0; i<3; i++)
        cout<<*(r.mutable_begin()+i)<<" "; // 50 42 43

*/

