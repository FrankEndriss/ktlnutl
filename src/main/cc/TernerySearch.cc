
/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
constexpr ld EPS=0.0000001;
ld terns(ld l, ld r, function<ld(ld)> f) {
    while(l+EPS<r) {
        const ld l3=l+(r-l)/3;
        const ld r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    return (l+r)/2;
};
