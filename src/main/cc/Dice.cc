
constexpr int DMIN=1;
constexpr int DMAX=6;
/*
 * Calls cb foreach possible dice combination, where mul is the number
 * of such combinations.
 * @param dice vector of size DMAX+1 with set dices so far, dice[i]= number of dices with value i
 * @param mul number of that combinations so far.
 * @cnt number of dices to go
 * @dnext min dice value to go
 * @cb which is called foreach possible combination of dices.
 */
void combies(vi &dice, const int mul, const int cnt, const int dnext, function<void(vi&,int)> cb)  {
    if(dnext==DMAX) {
        dice[dnext]=cnt;
        cb(dice,mul);
        dice[dnext]=0;
    } else {
        combies(dice, mul, cnt, dnext+1, cb); /* add zero dnext dices */

        for(int j=1; j<=cnt; j++) { /* add j dnext dices */
            dice[dnext]=j;
            if(cnt==j)
                cb(dice,mul);
            else {
                combies(dice, mul*nCr(cnt,j), cnt-j, dnext+1, cb);
            }
            dice[dnext]=0;
        }
    }
}

