
/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

template<typename T>
using indexed_set=tree<long long,null_type,less<long long>,rb_tree_tag, tree_order_statistics_node_update>;

/***************
use like

    indexed_set<int> is;
    is.insert(41);
    is.insert(42);

    auto x=*is.find_by_order(1); // random access
    cout<<x<<endl;   // 42
    cout<<is.order_of_key(41)<<endl;   // 0

    // If the element does not appear in the set, we get the position that the element
    // would have in the set:
    cout<<is.order_of_key(43)<<endl;   // 2

*/

