
#include <atcoder/lazysegtree>
using namespace atcoder;


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return min(f,x);
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return min(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return INF;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

