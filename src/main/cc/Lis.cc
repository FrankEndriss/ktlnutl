
constexpr int INF = 1e18;
vi idx;
/* auxilary list of lis, last element is p[ans], prelast is p[p[ans]] etc while p[ans]>=0 */
vi p;

/* see https://cp-algorithms.com/sequences/longest_increasing_subsequence.html
 *  * @return length of longest increasing subseq */
int lis(vector<int> const& a) {
    const int n = a.size();
    vector<int> d(n+1, INF);
    d[0] = -INF;
    p.resize(max(idx.size(), a.size()+1));
    idx.resize(max(idx.size(), a.size()+1));
    idx[0]=-1;

    int ans = 0;
    for (int i=0; i<n; i++) {
        int j = lower_bound(d.begin(), d.end(), a[i]) - d.begin();
        d[j] = a[i];
        idx[j]=i;
        p[i]=idx[j-1];

        if(j>ans)
            ans=j;
    }

    return ans;
}

