
/* Max-Flow with Edmonds-Karp
 * see https://cp-algorithms.com/graph/edmonds_karp.html */
const int INF = 1e9;

int bfs(int s, int t, vi &parent, vvi &adj, vvi &capacity) {
	fill(parent.begin(), parent.end(), -1);
	parent[s] = -2;
	queue<pii> q;
	q.push( { s, INF });

	while (!q.empty()) {
		int cur = q.front().first;
		int flow = q.front().second;
		q.pop();

		for (int next : adj[cur]) {
			if (parent[next] == -1 && capacity[cur][next]) {
				parent[next] = cur;
				int new_flow = min(flow, capacity[cur][next]);
				if (next == t)
					return new_flow;
				q.push( { next, new_flow });
			}
		}
	}

	return 0;
}

/* s==source
 * t==sink
 * n==adj.size()
 * adj is standart adjency matrix
 * NOTE: capacity is n*n matrix, only positive capacity is inserted
 * NOTE: adj is unidirected, ie both directions must be inserted!
 */
int maxflow(int s, int t, vvi &adj, vvi &capacity, int n) {
	int flow = 0;
	vi parent(n);
	int new_flow;

	while ((new_flow = bfs(s, t, parent, adj, capacity))) {
		flow += new_flow;
		int cur = t;
		while (cur != s) {
			int prev = parent[cur];
			capacity[prev][cur] -= new_flow;
			capacity[cur][prev] += new_flow;
			cur = prev;
		}
	}

	return flow;
}
