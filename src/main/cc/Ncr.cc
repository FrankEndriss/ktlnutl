
/** good for up to ~100 */
ll simple_nCr(int n, int k) {
    double res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;
    return (ll)(res + 0.01);
}


/** faster solution */
const int MAXN = 100;
ll C[MAXN + 1][MAXN + 1];

const bool init_nCr=[]() {
    C[0][0] = 1;
    for (int n = 1; n <= MAXN; ++n) {
        C[n][0] = C[n][n] = 1;
        for (int k = 1; k < n; ++k)
            C[n][k] = C[n - 1][k - 1] + C[n - 1][k];
    }
    return true;
}();

ll nCr(int n, int k) {
    return C[n][k];
}


