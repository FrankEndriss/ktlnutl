
using cd = complex<double>;

void fft(vector<cd> &a, bool invert) {
	int n = a.size();
	if (n == 1)
		return;

	vector<cd> a0(n / 2), a1(n / 2);
	for (int i = 0; 2 * i < n; i++) {
		a0[i] = a[2 * i];
		a1[i] = a[2 * i + 1];
	}
	fft(a0, invert);
	fft(a1, invert);

	double ang = 2 * PI / n * (invert ? -1 : 1);
	cd w(1, 0), wn(cos(ang), sin(ang));
	for (int i = 0; 2 * i < n; i++) {
		a[i] = a0[i] + w * a1[i];
		a[i + n / 2] = a0[i] - w * a1[i];
		if (invert) {
			a[i] /= 2;
			a[i + n / 2] /= 2;
		}
		w *= wn;
	}
}

/* see https://cp-algorithms.com/algebra/fft.html 
 *
 * Freq of all sums of two sets of nubmers:
 * https://cp-algorithms.com/algebra/fft.html#toc-tgt-9
 * */
void multiply(vector<int> const &a, vector<int> const &b, vector<int> &result) {
	vector<cd> fa(a.begin(), a.end()), fb(b.begin(), b.end());
	size_t n = 1;
	while (n < a.size() + b.size())
		n <<= 1;
	fa.resize(n);
	fb.resize(n);

	fft(fa, false);
	fft(fb, false);
	for (size_t i = 0; i < n; i++)
		fa[i] *= fb[i];
	fft(fa, true);

	result.assign(n, 0);
	for (size_t i = 0; i < n; i++)
		result[i] = round(fa[i].real());
}

/* see also
 * https://ikatanic.github.io/2018/09/22/polynomial-multiplication/
 * https://de.wikipedia.org/wiki/Sch%C3%B6nhage-Strassen-Algorithmus
 * https://everything2.com/title/Strassen+algorithm+for+polynomial+multiplication
 */
