
/* Persistent segment tree.
 * Init node as copy of existing root, then update as needed.
 * Note that later updates affect all derived roots.
 */
struct node {
    node *left;
    node *right;
    int val;

    bool ownL=false;
    bool ownR=false;

    node() {
        left=0;
        right=0;
        val=0;
    }

    /* sum query */
    int query(int sL, int sR, int l, int r) {
        if(l>sR || r<sL)
            return 0;
        //cerr<<"query, sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<endl;

        if(l<=sL && r>=sR)
            return val;

        int mid=(sL+sR)/2;
        return left->query(sL,mid,l,r) + right->query(mid+1,sR,l,r);
    }

    /* point update on copy */
    void set(int sL, int sR, int idx, int v) {
        //cerr<<"set, sL="<<sL<<" sR="<<sR<<" idx="<<idx<<" v="<<v<<endl;
        if(sL==sR) {
            assert(sL==idx);
            val=v;
            return;
        }
        assert(idx>=sL && idx<=sR);

        int  mid=(sL+sR)/2;
        if(idx<=mid) {
            if(!ownL) {
                node *nleft=new node();
                nleft->left=left->left;
                nleft->right=left->right;
                nleft->val=left->val;
                ownL=true;
                left=nleft;
            }
            left->set(sL,mid,idx,v);

        } else {
            if(!ownR) {
                node *nright=new node();
                nright->left=right->left;
                nright->right=right->right;
                nright->val=right->val;
                ownR=true;
                right=nright;
            }
            right->set(mid+1,sR,idx,v);
        }
        val=left->val + right->val;
    }
};

