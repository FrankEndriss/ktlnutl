/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;


/* filter windows lineends */
void mygetline(string &s) {
    s.clear();
    int c=getchar_unlocked();
    while(c!='\n' && c>=0) {
        if(c!=13)
            s.push_back((char)c);
        c=getchar_unlocked();
    }
}

/* see https://www.geeksforgeeks.org/fast-io-for-competitive-programming/ */
inline void fs(int &number) {
    bool negative = false;

    /* see https://www.geeksforgeeks.org/getchar_unlocked-faster-input-cc-competitive-programming/ */
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == '\t' || c == 13 || c == 10)
        c = gc();

    if (c == '-') {
        negative = true;
        c = gc();
    }

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }

    if (negative)
        number = -number;
}

/* see http://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html
 * We could make it even faster using 3 (or 4?) digits per division. */
const char digits[] =
    "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(ll n) {
    char buf[32];
    int idx = 0;

    if (n == 0) {
        pc('0');
        return;
    }

    while (n >= 100) {
        ll val = n % 100;
        buf[idx++] = digits[val * 2 + 1];
        buf[idx++] = digits[val * 2];
        n /= 100;
    }

    while (n) {
        buf[idx++] = n % 10 + '0';
        n /= 10;
    }

    while (idx--) {
        pc(buf[idx]);
    }
}

typedef vector<string> vs;

/* Split a string into parts by separator */
vs split(const string &s, const string &sep) {
    vs ans;
    size_t ppos = 0;
    while (true) {
        size_t pos = s.find(sep, ppos);
        if (pos != string::npos) {
            ans.push_back(s.substr(ppos, pos - ppos));
            ppos = pos + sep.size();
        } else
            break;
    }
    if (ppos < s.size())
        ans.push_back(s.substr(ppos));
    return ans;
}

/** sets ':' as additional delimiter for getline */
struct colon_is_space: std::ctype<char> {
    colon_is_space() :
        std::ctype<char>(get_table()) {
    }
    static mask const* get_table() {
        static mask rc[table_size];
        rc[' '] = std::ctype_base::space;
        rc[':'] = std::ctype_base::space;
        rc['\n'] = std::ctype_base::space;
        return &rc[0];
    }
};

/* read until custom delimiter
 * @return false on eof (nothing readed) */
bool some_parse_func(string &str) {
    return getline(cin, str, cin.widen(':'))
}

/* example how to setup getline */
int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cin.imbue(locale(cin.getloc(), new colon_is_space));
}

