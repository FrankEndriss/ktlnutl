
/* see https://cp-algorithms.com/graph/finding-cycle.html
 * Note that this finds a cylce of size 2. To skip cylces of size 2
 * add a parent parameter to dfs(v,p).
 *
 * For directed graph modification see https://cses.fi/problemset/task/1678/
 * It adds a check so that in first place the dfs is started for 'root'
 * vertex only, that are vertex without an incoming edge.
 **/

vector<vector<int>> adj;
vector<char> color;
vector<int> parent;
int cycle_start, cycle_end;
vector<int> cycle;

bool dfs(int v) {
    color[v] = 1;
    for (int u : adj[v]) {
        if (color[u] == 0) {
            parent[u] = v;
            if (dfs(u))
                return true;
        } else if (color[u] == 1) {
            cycle_end = v;
            cycle_start = u;
            return true;
        }
    }
    color[v] = 2;
    return false;
}

/* set graph in adj[][], result cycle found in cycle[] */
void find_cycle(int n) {
    color.assign(n, 0);
    parent.assign(n, -1);
    cycle_start = -1;

    for (int v = 0; v < n; v++) {
        if (color[v] == 0 && dfs(v))
            break;
    }

    if (cycle_start != -1) {
        cycle.push_back(cycle_start);
        for (int v = cycle_end; v != cycle_start; v = parent[v])
            cycle.push_back(v);
        cycle.push_back(cycle_start);
        reverse(cycle.begin(), cycle.end());
    }
}
