
/* Add a value at each position in a range in O(logn),
 * query a value at position in O(logn)
 * E has to implement operator+=
 **/
template <typename E>
struct RangeUpdate {
    int n;
    vector<E> a;
    RangeUpdate(int _n) {
        n=1;
        while(n<_n)
            n*=2;

        a.resize(2*n);
    }

    void add(int node, int segL, int segR, int l, int r, E val) {
        if(l==segL && r==segR) {
            a[node]+=val;
            return;
        }

        int mid=(segL+segR)/2;
        if(l<mid)
            add(node*2, segL, mid, l, min(mid, r), val);

        if(r>mid)
            add(node*2+1, mid, segR, max(mid, l), r, val);
    }

    /* add val to all positions in interval (l,r] */
    void add(int l, int r, E val) {
        add(1, 0, n, l, r, val);
    }

    int get(int node, int segL, int segR, int idx) {
        int ans=a[node];
        if(segL+1==segR) {
            assert(segL==idx);
        } else {
            int mid=(segL+segR)/2;
            if(idx<mid)
                ans+=get(node*2, segL, mid, idx);
            else
                ans+=get(node*2+1, mid, segR, idx);
        }
        return ans;
    }

    E get(int idx) {
        return get(1, 0, n, idx);
    }
};
