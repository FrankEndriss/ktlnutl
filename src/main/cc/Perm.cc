
/* works like next_permutation(), but on a permutation like seq in p, with max value i.
 * usually i != p.size(); 
 * Usefull to create all subsets of a given size of a set of values.
 * vi data(n);  // some data...
 * vi idx(m);   // subset of size m; m<=n;
 * iota(idx.begin(), idx.end(), 0);
 * do {
 *      ...something using data readonly...
 * } while(next(idx, n-1));
 *
 * example for m=3, n=4:
 * 0 1 2 
 * 0 1 3 
 * 0 2 3 
 * 1 2 3
 **/
bool next(vi &p, int i) {
    const int k=(int)p.size();

    if(p[k-1]<i) {
        p[k-1]++;
        return true;
    }
    if(k==1)
        return false;

    int incidx=k-2;
    while(incidx>=0 && p[incidx]==p[incidx+1]-1)
        incidx--;

    if(incidx<0)
        return false;

    p[incidx]++;
    for(int j=incidx+1; j<k; j++)
        p[j]=p[j-1]+1;

    return true;
}

