
/* Known as the prefix function. See
 * https://cp-algorithms.com/string/prefix-function.html
 * http://algorithmsforcontests.blogspot.com/2012/08/borders-of-string.html
 * @return len of border pre/postfix in O(n) */
int calculate_border(string& s) {
    int i = 1;
    int j = -1;
    int n = (int)s.size();

    vi border(s.size());
    border[0] = -1;
    while(i < n) {
        while(j >= 0 && s[i] != s[j+1])
            j = border[j];
        if (s[i] == s[j+1])
            j++;
        border[i++] = j;
    }
    return border.back()+1;
}


/* https://stackoverflow.com/questions/3833103/longest-palindrome-prefix */
const int mod1=1e9+7;
const int mod2=1e9+9;
const int base1=1e6+7;
const int base2=1e6+9;

int lprefix(string& a) {
    int n=a.size();
    int match = n - 1;
    int ha1 = 0, ha2 = 0, hr1 = 0, hr2 = 0;
    int m1 = 1, m2 = 1;
    for ( int i = 0; i<n; ++i ) {
        ha1 = (ha1 + m1*a[i]) % mod1;
        ha2 = (ha2 + m2*a[i]) % mod2;

        hr1 = (a[i] + base1*hr1) % mod1;
        hr2 = (a[i] + base2*hr2) % mod2;

        m1 *= base1, m1 %= mod1;
        m2 *= base2, m2 %= mod2;

        if ( ha1 == hr1 && ha2 == hr2 )
            match = i;
    }
    return match+1;
}

