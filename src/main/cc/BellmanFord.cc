
/* Bellman-Ford, find a negative cycle in oriented graph.
 * see https://cp-algorithms.com/graph/finding-negative-cycle-in-graph.html
 */

struct BFEdge {
    int a, b, cost;
};

/* return a vector with vertex of a negative cycle,
 * or empty vector for 'no negative cylcle exists'.
 * n is number of vertex
 */
vector<int> solve(const int n, vector<BFEdge> &edges) {
    vector<int> d(n);
    vector<int> p(n, -1);
    int x;
    for (int i = 0; i < n; ++i) {
        x = -1;
        for (BFEdge e : edges) {
            if (d[e.a] + e.cost < d[e.b]) {
                d[e.b] = d[e.a] + e.cost;
                p[e.b] = e.a;
                x = e.b;
            }
        }
    }

    vector<int> ans;
    if (x == -1) {
        return ans;
    } else {
        for (int i = 0; i < n; ++i)
            x = p[x];

        for (int v = x;; v = p[v]) {
            ans.push_back(v);
            if (v == x && ans.size() > 1)
                break;
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
}
