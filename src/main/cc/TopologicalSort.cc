
/*
 * see https://cp-algorithms.com/graph/topological-sort.html
 */

vector<bool> visited;
vector<int> ans;

void dfs(vvi& adj, int v) {
    visited[v] = true;
    for (int u : adj[v]) {
        if (!visited[u])
            dfs(u);
    }
    ans.push_back(v);
}

vi topological_sort(vvi& adj, int n) {
    visited.assign(n, false);
    ans.clear();
    for (int i = 0; i < n; ++i) {
        if (!visited[i])
            dfs(adj, i);
    }
    reverse(ans.begin(), ans.end());
    return ans;
}
