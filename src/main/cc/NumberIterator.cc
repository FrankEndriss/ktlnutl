
struct NumberIterator : iterator<forward_iterator_tag, ll> {
    ll v;
    NumberIterator(ll _v) : v(_v) {}
    operator ll&() { return v; }
    ll operator*() { return v; }
};
 
struct range : pii {
    range(ll begin, ll end) : pii(begin, max(begin, end)) {} /* exclusive end */
    range(ll n) : pii(0LL, max(0LL, n)) {}
    NumberIterator begin() { return first; }
    NumberIterator end() { return second; }
};
