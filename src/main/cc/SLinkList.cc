
/* Single linked list/tree.
 * Basically you can create new root nodes,
 * and childs of existing nodes.
 * see append(...) and remove(...)
 * So any node is identified by its int id and has a parent.
 * Any node holds a copy of some data of type T.
 * */
template<typename T>
struct slistent {
    T data;
    int prev;
};

template<typename T>
struct slist {
    vector<slistent<T>> entries;
    stack<int> stfree;
    vector<int> refcnt; /* revcnt[i]= number of children of node i */

/* Appends a new node to node prev, return the nodeID of the new node.
 * @param prev The parent node, use <0 to create a new root.
 * @param value The value associated with the new node.
 * @return nodeID of the new node.
 */
    int append(int prev, T value) {
        int nent=-1;
        if(stfree.size()) {
            nent=stfree.top();
            stfree.pop();
            entries[nent].data=value;
            entries[nent].prev=prev;
        } else {
            nent=(int)entries.size();
            entries.push_back({value,prev});
            refcnt.push_back(0);
        }
        if(prev>=0)
            refcnt[prev]++;
        return nent;
    }

/* Remove a node from the list/tree.
 * @param ent nodeID of the node to remove. It should be a leaf. If not, all children
 * of ent will reference a non existing node afterwards.
 * @return The refcount of the parent of ent.
 */
    int remove(int ent) {
        stfree.push(ent);
        if(entries[ent].prev>=0) {
            refcnt[entries[ent].prev]--;
            return refcnt[entries[ent].prev];
        } else
            return -1;
    }

/** Removes a path from the tree. ie removes ent and the parent of ent
 * (recursively) until a parent has more than the one removed child.
 */
    void removePath(int ent) {
        while(ent>=0 && remove(ent)==0)
            ent=entries[ent].prev;
    }

/** Get are reference to slistent of node with id ent.
 * #return the node with id ent. Such a node will not be changed until
 * it is removed from the list/tree.
 */
    slistent<T>& get(int ent) {
        return entries[ent];
    }
};


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    slist<int> sl;
    int root=sl.append(-1, 1000);
    int n1=sl.append(root, 1001);
    int n2=sl.append(n1, 1002);
    int n11=sl.append(n1, 1011);
    int n12=sl.append(n11, 1012);

    int ent=n12;
    while(ent>=0) {
        auto sent=sl.get(ent);
        cout<<sent.data<<" ";
        ent=sent.prev;
    }
    cout<<endl;
}

