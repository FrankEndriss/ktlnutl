
int extended_euclidean(int a, int b, int& x, int& y) {
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int x1, y1;
    int d = extended_euclidean(b, a % b, x1, y1);
    x = y1;
    y = x1 - y1 * (a / b);
    return d;
}

/* Inverse based on ext gcd, -1 for not exist */
int inv(const int a, const int mod) {
    int x, y;
    int g = extended_euclidean(a, mod, x, y);
    if (g != 1) {
        return -1;
    } else {
        return (x % mod + mod) % mod;
    }
}

/*
 * Linear Congruence Equation, see
 * https://cp-algorithms.com/algebra/linear_congruence_equation.html
 *
 * It is that if
 * a*x=b        |mod n
 * Then:
 * x=b*inv(a)   |mod n
 * -1 for no solution
 */
int lce(int a, int b, int mod) {
    int g=gcd(a,mod);
    if(g!=1) {
        if(b%g!=0)
            return -1LL;    /* no solution */

        a/=g;
        b/=g;
        mod/=g;
    }
    return (b*inv(a, mod))%mod;
}
