
#include <atcoder/modint>
using namespace atcoder;

const int INF = 2;

//using mint=modint;
constexpr int MOD=1e9+7;
using mint=static_modint<MOD>;


/* see https://cp-algorithms.com/linear_algebra/linear-system-gauss.html
 * @return number of solutoins: 0,1 or INF
 * if 1 then the solution is contained in ans.
 **/
int gauss(vector<vector<mint>> a, vector<mint> &ans) {
    int n = (int) a.size();
    int m = (int) a[0].size() - 1;

    vector<int> where (m, -1);
    for (int col=0, row=0; col<m && row<n; ++col) {
        int sel = row;
        for (int i=row; i<n; ++i)
            if(a[i][col].val() > a[sel][col].val())
                sel = i;

        if (a[sel][col]==0)
            continue;

        for (int i=col; i<=m; ++i)
            swap (a[sel][i], a[row][i]);

        where[col] = row;

        for (int i=0; i<n; ++i)
            if (i != row) {
                auto c = a[i][col] / a[row][col];
                for (int j=col; j<=m; ++j)
                    a[i][j] -= a[row][j] * c;
            }
        ++row;
    }

    ans.assign (m, mint(0));
    for (int i=0; i<m; ++i)
        if (where[i] != -1)
            ans[i] = a[where[i]][m] / a[where[i]][i];

    for (int i=0; i<n; ++i) {
        mint sum = 0;
        for (int j=0; j<m; ++j)
            sum += ans[j] * a[i][j];
        if (sum - a[i][m] !=0)
            return 0;
    }

    for (int i=0; i<m; ++i)
        if (where[i] == -1)
            return INF;

    return 1;
}

