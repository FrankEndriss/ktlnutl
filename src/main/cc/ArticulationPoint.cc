/* find if there is an articulation point in a component.
 *  * see https://cp-algorithms.com/graph/cutpoints.html */
vector<vector<int>> adj;
bool found;

vector<bool> visited;
vector<int> tin, low;
int timer;

void dfs(int v, int p = -1) {
    visited[v] = true;
    tin[v] = low[v] = timer++;
    int children=0;
    for (int to : adj[v]) {
        if (to == p)
            continue;
        if (visited[to]) {
            low[v] = min(low[v], tin[to]);
        } else {
            dfs(to, v);
            low[v] = min(low[v], low[to]);
            if (low[to] >= tin[v] && p!=-1) {
                /* cerr<<"found, v="<<v<<endl; */
                found=true;
            }
            ++children;
        }
    }
    if(p == -1 && children > 1) {
        /* cerr<<"found, v="<<v<<endl; */
        found=true;
    }
}

void find_cutpoints() {
    timer = 0;
    visited.assign(adj.size(), false);
    tin.assign(adj.size(), -1);
    low.assign(adj.size(), -1);
    for (size_t i = 0; i < adj.size(); ++i) {
        if (!visited[i])
            dfs (i);
    }
}

