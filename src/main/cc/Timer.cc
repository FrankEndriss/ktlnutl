
const int64_t CYCLES_PER_SEC = 3000000000;
const double TIMELIMIT = 2.95;
struct Timer {
    int64_t start;
    Timer() {
        reset();
    }
    void reset() {
        start = getCycle();
    }
    void plus(double a) {
        start -= (a * CYCLES_PER_SEC);
    }
    /** Time in seconds */
    inline double get() {
        return (double)(getCycle() - start) / CYCLES_PER_SEC;
    }
    inline int64_t getCycle() {
        uint32_t low, high;
        __asm__ volatile("rdtsc"
                         : "=a"(low), "=d"(high));
        return ((int64_t)low) | ((int64_t)high << 32);
    }
};
