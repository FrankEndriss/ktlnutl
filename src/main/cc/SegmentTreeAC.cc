
#include <atcoder/segtree>
using namespace atcoder;


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return min(a,b);
}

const S INF=1e9;
S st_e() {
    return INF;
}

using stree=segtree<S, st_op, st_e>;

