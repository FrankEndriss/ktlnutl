
/* some operator overloading for sets operations 
 * -, ^, |, & 
 * minus, xor, or, and
 **/
template <typename T>
set<T> to_set(const vector<T> s) {
    set<T> ans;
    copy(all(s), inserter(ans, ans.end()));
    return ans;
}

template <typename T>
vector<T> to_vector(const set<T> s) {
    vector<T> ans;
    copy(all(s), back_inserter(ans));
    return ans;
}

template <typename T>
set<T> operator-(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_difference(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator-=(set<T> &s1, const set<T> &s2) {
    return (s1=s1-s2);
}

template <typename T>
set<T> operator^(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_symmetric_difference(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator^=(set<T> &s1, const set<T> &s2) {
    return (s1=s1^s2);
}

template <typename T>
set<T> operator|(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_union(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator|=(set<T> &s1, const set<T> &s2) {
    return (s1=s1|s2);
}

template <typename T>
set<T> operator&(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_intersection(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator&=(set<T> &s1, const set<T> &s2) {
    return (s1=s1&s2);
}

/*
void solve() {
    set<string> s1;
    set<string> s2;
    s1.insert(string("hello1"));
    s1.insert(string("hello2"));
    s2.insert(string("hello2"));
    s2.insert(string("hello3"));

    set<string> ans=s1&s2;
    assert(ans.size()==1);

    ans=s1|s2;
    assert(ans.size()==3);

    ans=s1^s2;
    assert(ans.size()==2);

    ans=s1-s2;
    assert(ans.size()==1);
    assert(string("hello1")==*ans.begin());

    set<string> ss1=s1;
    set<string> ss2=s2;
    ss1&=ss2;
    assert(ss1.size()==1);
}

signed main() {
    solve();
}
*/
