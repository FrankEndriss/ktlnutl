
/* https://cp-algorithms.com/algebra/extended-euclid-algorithm.html
 * result: a*x + b*y = gcd(a, b)
 */
ll extGcd(ll a, ll b, ll &x, ll &y) {
    if (a == 0) {
        x = 0;
        y = 1;
        return b;
    }
    ll x1, y1;
    ll d = extGcd(b % a, a, x1, y1);
    x = y1 - (b / a) * x1;
    y = x1;
    return d;
}

/* https://cp-algorithms.com/algebra/linear-diophantine-equation.html
 * result: a*x + b*y = c
 * Returns true if a solution exists, and x and y and g=gcd(a, b).
 */
bool find_any_diophantine(ll a, ll b, ll c, ll &x, ll &y, ll &g) {
    g = extGcd(abs(a), abs(b), x, y);
    if (c % g) {
        return false;
    }

    x *= c / g;
    y *= c / g;
    if (a < 0) x = -x;
    if (b < 0) y = -y;
    return true;
}

