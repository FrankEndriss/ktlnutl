/* SCC strongly connected components
 * see https://www.geeksforgeeks.org/strongly-connected-components/
 */


void DFSUtil(int v, vb& vis, vvi& adj, vi& ans) {
    vis[v] = true;
    ans.push_back(v);

    for(int i : adj[v])
        if(!vis[i])
            DFSUtil(i, vis, adj, ans);
}

void fillOrder(int v, vb& vis, stack<int> &Stack, vvi& adj) {
    vis[v] = true;

    for(int i : adj[v])
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    Stack.push(v);
}

/* The main function that finds returns all strongly connected components */
vvi printSCCs(vvi& adj) {
    stack<int> Stack;
    vb vis(adj.size());

    /* Fill vertices in stack according to their finishing times */
    for(size_t i = 0; i<adj.size(); i++)
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    /* Create a reversed graph */
    vvi adjT(adj.size());
    for (size_t v=0; v<adj.size(); v++) {
        for(int i : adj[v])
            adjT[i].push_back(v);
    }

    vis.assign(vis.size(), false);

    vvi ans;
    while(Stack.size()) {
        int v = Stack.top();
        Stack.pop();

        if (!vis[v]) {
            ans.resize(ans.size()+1);
            DFSUtil(v, vis, adjT, ans.back());
        }
    }
    return ans;
}
