
/* see https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
 * The Bron–Kerbosch algorithm is an algorithm for finding all maximal cliques in an undirected graph.
 * O(n^(n/3))
 * All sets are implemented as bitsets, since N should be small anyway.
 */
const int N=100;
/* @return vector of all maximum sized maximal sets, see reportResult */
vector<bitset<N>> BronKerboschPivot(const vector<bitset<N>> &adj) {
    const int n=adj.size();
    vector<bitset<N>> ans;

    /* called whenever a clique is found */
    function<void(bitset<N>&)> reportResult=[&](bitset<N> &cliq) {
        //cerr<<"reportResult, cliq="<<cliq<<endl;
        const int cnt=cliq.count();
        if(ans.size()) {
            const int cnt0=ans[0].count();
            if(cnt0<cnt)
                ans.clear();
            else if(cnt0>cnt)
                return;
        }
        ans.push_back(cliq);
    };

    function<void(bitset<N>&,bitset<N>&,bitset<N>&)> execute=[&](bitset<N> &r, bitset<N> &p, bitset<N> &x) {
        //cerr<<"execute"<<endl<<"r="<<r<<endl<<"p="<<p<<endl<<"x="<<x<<endl;;
        if(p.count()==0 && x.count()==0) {
            reportResult(r);
            return;
        }

        /* find pivot as the vertex from p united x with most neighbours. */
        bitset<N> pUx=p|x;
        int pivot=-1;
        int man=-1;
        for(int i=0; i<n; i++) {
            if(!pUx[i])
                continue;

            int cnt=adj[i].count();
            if(cnt>man) {
                man=cnt;
                pivot=i;
            }
        }

        //cerr<<"using pivot="<<pivot<<" adj.size()="<<adj.size()<<endl;

        for(int i=0; i<n; i++) {    /* for all vertex in p except the neighbours of pivot, do... */
            //cerr<<"i="<<i<<endl;
            //cerr<<"adj[pivot][i]="<<adj[pivot][i]<<" !p[i]="<<!p[i]<<endl;
            if(!p[i] || adj[pivot][i])
                continue;

            bitset<N> rr=r;
            rr.set(i);
            bitset<N> pp=p&adj[i];
            bitset<N> xx=x&adj[i];
            execute(rr, pp, xx);
        }
    };

    bitset<N> rr;
    bitset<N> pp;
    bitset<N> xx;
    for(int i=0; i<n; i++) 
        pp.set(i);

    execute(rr,pp,xx);
    return ans;
}
