
/* Maintains arbitrary large integer as
 * single "digits" of base 1e9 in a
 * long long array.
 * The base 1e9 allows us to quickly convert to decimal.
 * Smallest significant digit at position 0.
 */
constexpr ll BASE=1e9;
struct bigint {
    vector<ll> data;
    bigint(ll i=0) {
        while(i) {
            data.push_back(i%BASE);
            i/=BASE;
        }
    }

    bigint& operator+=(const bigint& rhs) {
        for(int i=0; i<rhs.data.size(); i++) {
            if(data.size()<i+1)
                data.push_back(0);
            data[i]+=rhs.data[i];
            int j=i;
            while(data[j]>BASE) {
                if(data.size()==j)
                    data.push_back(0);
                data[j+1]+=data[j]/BASE;
                data[j]%=BASE;
            }
        }
        _norm(data);
        return *this;
    }

    friend bigint operator+(bigint lhs, const bigint &rhs) {
        lhs+=rhs;
        return lhs;
    }

    bigint& operator*=(const bigint& rhs) {
        data=_mul(data, rhs.data);
        _norm(data);
        return *this;
    }

    friend bigint operator*(bigint lhs, const bigint &rhs) {
        lhs*=rhs;
        return lhs;
    }

    vll _mul(const vll &a, const vll &b) {
        vll ans(a.size()+b.size()+1);
        for(size_t i=0; i<b.size(); i++) {
            for(size_t j=0; j<a.size(); j++) {
                ans[i+j]+=b[i]*a[j];
                int k=i+j;
                while(ans[k]>BASE) {
                    ans[k+1]+=ans[k]/BASE;
                    ans[k]%=BASE;
                    k++;
                }
            }
        }
        _norm(ans);
        return ans;
    }

    void _norm(vll &a) {
        while(a.size() && a.back()==0)
            a.pop_back();
    }

    bool operator<(const bigint &rhs) {
        if(data.size()!=rhs.data.size())
            return data.size()<rhs.data.size();

        for(int i=data.size()-1; i>=0; i--)
            if(data[i]!=rhs.data[i])
                return data[i]<rhs.data[i];

        return false;
    }
};

struct bifract {
    bigint nom;
    bigint denom;

    bifract(ll _nom=1, ll _denom=1) {
        nom=bigint(_nom);
        denom=bigint(_denom);
    }

    bifract& operator+=(const bifract& rhs) {
        nom=(nom*rhs.denom) + (denom*rhs.nom);
        denom*=rhs.denom;
        return *this;
    }

    friend bifract operator+(bifract lhs, const bifract &rhs) {
        lhs+=rhs;
        return lhs;
    }

    bifract& operator*=(const bifract& rhs) {
        nom*=rhs.nom;
        denom*=rhs.denom;
        return *this;
    }

    friend bifract operator*(bifract lhs, const bifract &rhs) {
        lhs*=rhs;
        return lhs;
    }

    bool operator<(const bifract &rhs) {
        return (nom*rhs.denom) < (denom*rhs.nom);
    }
};

