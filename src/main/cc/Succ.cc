
/* Structure to efficiently access Kth successor of some element in linked lists.
 * Based on vector. */
struct Succ {
    vector<vector<int>> succIndex;
    vector<int> log;
    /* @param startV start of values. Values must be >=0
     * @param endV end of values
     * @param maxV maximum Value
     * @param maxK maximum length of contained list, max value for query successors
     * @param succFunc function which returns successor of arg. Should return -1 for no successor.
     */
    Succ(auto begin, auto end, int maxV, int maxK, function<int(int)> succFunc) {
        log.resize(maxK+1);
        for(int i=1; i<maxK+1; i++)
            log[i]=log[i/2]+1;

        int mxLog=log[maxK];
        succIndex.resize(maxK+1, vector<int>(mxLog, -1));

        for(; begin!=end; begin++) {
            int i=*begin;
            succIndex[i][0]=succFunc(i);
        }

        int k=2;
        while((k*2-1)<maxK) {
            for(int i=0; i<=maxV; i++) {
                int lsucc=succIndex[i][log[k]-1];
                if(lsucc>=0)
                    succIndex[i][k]=succIndex[lsucc][log[k]-1];
            }
            k<<=1;
        }
    }

    /* @return Kth successor of v */
    int succ(int v, int k) {
        while(k>0 && v>=0) {
//cout<<"succ v="<<v<<" k="<<k<<" log[k]-1="<<log[k]-1<<endl;
            v=succIndex[v][log[k]-1];
            k-=1<<(log[k]-1);
        }
        return v;
    }
};
