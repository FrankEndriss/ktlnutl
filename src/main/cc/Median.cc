
/* add/remove elements O(logn), get median(),cost() in O(1) 
 * Note: Implementation using priority_queue instead of multiset is not really faster.
 **/
struct Median {
    multiset<int> lo;    // smaller elements
    multiset<int> hi;    // bigger elements
    int sumlo=0;
    int sumhi=0;

    void norm() {
        while(lo.size()+1<hi.size()) {
            auto it=hi.begin();
            sumlo+=*it;
            sumhi-=*it;
            lo.insert(*it);
            hi.erase(it);
        }

        while(lo.size()>hi.size()) {
            auto it=lo.end();
            it--;
            sumlo-=*it;
            sumhi+=*it;
            hi.insert(*it);
            lo.erase(it);
        }

        while(lo.size()>0 && *lo.rbegin()>*hi.begin()) {
            auto itlo=lo.end();
            itlo--;
            hi.insert(*itlo);
            sumhi+=*itlo;
            sumlo-=*itlo;
            lo.erase(itlo);
            auto it=hi.begin();
            lo.insert(*it);
            sumlo+=*it;
            sumhi-=*it;
            hi.erase(it);
        }
    }

    void add(int a) {
        if(lo.size()<hi.size()) {
            lo.insert(a);
            sumlo+=a;
        } else {
            hi.insert(a);
            sumhi+=a;
        }
        norm();
    }

    void remove(int a) {
        if(a<*hi.begin()) {
            auto it=lo.find(a);
            assert(it!=lo.end());
            sumlo-=*it;
            lo.erase(it);
        } else {
            auto it=hi.find(a);
            assert(it!=hi.end());
            sumhi-=*it;
            hi.erase(it);
        }
        norm();
    }

    int median() {
        return *(hi.begin());
    }

    int cost() {
        int med=median();
        return med*lo.size()-sumlo + sumhi-med*hi.size();
    }

};
