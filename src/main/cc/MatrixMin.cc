
const int INF=2e18+7;

/* matrix multiplication meets floyd_warshal.
 * see https://cp-algorithms.com/graph/fixed_length_paths.html */
vvi mulM(vvi &m1, vvi &m2) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz, INF));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                if(m1[i][k]!=INF && m2[k][j]!=INF)
                    ans[i][j]=min(ans[i][j], m1[i][k]+m2[k][j]);
            }
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi a, int p) {
    assert(p>0);
    vvi res=a;
    p--;

    while (p) {
        if (p & 1)
            res = mulM(res, a);
        p >>= 1;
        a = mulM(a, a);
    }
    return res;
}

