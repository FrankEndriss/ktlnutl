
/* see https://cp-algorithms.com/data_structures/sqrt_decomposition.html#toc-tgt-4
 * Queries are inclusive l and inclusive r.
 **/

//const int BLOCK_SIZE=(int)sqrt(5e5+5)+1;
const int BLOCK_SIZE=512;

bool mo_cmp(pair<int, int> p, pair<int, int> q) {
    if (p.first / BLOCK_SIZE != q.first / BLOCK_SIZE)
        return p < q;
    return (p.first / BLOCK_SIZE & 1) ? (p.second < q.second) : (p.second > q.second);
}

/* example impl of the three functions
 ** for finding number of numbers in
 ** interval where freq==number */
int gans;
vi f;
vi a;

inline void moremove(int idx) {
    if(--f[a[idx]]==0)
        gans--;
}

inline void moadd(int idx) {
    if(++f[a[idx]]==1)
        gans++;
}

inline int moget_answer() {
    return gans;
}

/* (l,r) both inclusive, 0 based */
struct Query {
    int l, r, idx;

    bool operator<(Query other) const {
        return mo_cmp(make_pair(l, r), make_pair(other.l, other.r));
    }
};

vector<int> mo(vector<Query>& queries) {
    vector<int> answers(queries.size());
    sort(queries.begin(), queries.end());
    int cur_l = 0;
    int cur_r = -1;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
    for (Query q : queries) {
        while (cur_l > q.l) {
            cur_l--;
            moadd(cur_l);
        }
        while (cur_r < q.r) {
            cur_r++;
            moadd(cur_r);
        }
        while (cur_l < q.l) {
            moremove(cur_l);
            cur_l++;
        }
        while (cur_r > q.r) {
            moremove(cur_r);
            cur_r--;
        }
        answers[q.idx] = moget_answer();
    }
    return answers;
}
