package com.happypeople.ktlnutl

class Conway {

    data class Cell(val x: Int, val y: Int);

    val field = mutableSetOf<Cell>();

    fun pointsArround(p: Cell): Set<Cell> {
        val ret = mutableSetOf<Cell>();
        (-1..1).map { itX ->
            (-1..1).map { itY ->
                if ((itX != 0 || itY != 0))
                    ret.add(Cell(itX, itY));
            }
        }
        return ret;
    }

}