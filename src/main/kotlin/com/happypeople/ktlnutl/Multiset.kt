package com.happypeople.codeforces

/** Multiset is like a Set, but elements can be multiple time in a Multiset, not only once like in a Set.
 * Basically it is a Map<E, Int>, where the Int acts as a counter.
 * The insert operation increments the counter, the remove decrements it.
 */
class Multiset<E> {
    private val map = mutableMapOf<E, Int>()

    public fun add(e: E) {
        val c = map.getOrDefault(e, 0)
        map[e] = c + 1
    }

    public fun remove(e: E) {
        val c = map.getOrDefault(e, 0)
        if (c > 1)
            map[e] = c - 1
        else if (c == 1)
            map.remove(e)
    }

    public fun contains(e: E) =
            map.contains(e)

    public fun count(e: E) =
            map.getOrDefault(e, 0)

    public fun toList(): List<E> {
        return map.flatMap { ent -> (1..ent.value).map { ent.key } }
    }

    public fun toSet(): Set<E> = map.keys
}