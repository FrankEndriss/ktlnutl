package com.happypeople.codeforces

// see also  https://www.hackerearth.com/practice/notes/segment-tree-and-lazy-propagation/

/** SegmentTree of Int. Note that you can hold your data in your own storage and give
 * an Array of indices to a SegmentTree.
 * @param a The initial data.
 * @param neutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
 * @param cumul The cumulative function to create the "sum" of two nodes.
 * */
//class SegmentTree(a: LongArray, val neutral: Long, val cumul: (Long, Long) -> Long) {
class SegmentTree<E>(a: List<E>, val neutral: E, val cumul: (E, E) -> E) {
    val tree = (1..(a.size * 2)).map { neutral }.toMutableList() // kotlin requires wired array initialization.
    private val treeDataOffset = a.size

    init {
        for (i in a.indices)
            tree[treeDataOffset + i] = a[i]

        for (i in treeDataOffset - 1 downTo 1)
            tree[i] = cumul(tree[leftChild(i)], tree[rightChild(i)])
    }

    /** Updates the data at dataIdx to value. */
    public fun update(dataIdx: Int, value: E) {
        // println("update dataIdx=$dataIdx value="+value)
        // println("bef update, tree=${tree.toList()}")
        var treeIdx = treeDataOffset + dataIdx
        tree[treeIdx] = value

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx)
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)])
        }
        // println("aft update, tree=${tree.toList()}")
    }

    /** @return the cumul(idxL, idxR), idxL inclusive, idxR exclusive. */
    public fun get(pIdxL: Int, pIdxR: Int): E {
        var idxL = pIdxL + treeDataOffset
        var idxR = pIdxR + treeDataOffset
        var first = true
        var cum = neutral
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL])
                idxL++
            }
            if (isOdd(idxR)) {
                idxR--
                cum = cumul(cum, tree[idxR])
            }
            idxL = parent(idxL)
            idxR = parent(idxR)
        }
        return cum
    }

    companion object {
        val ROOT = 1
        fun isOdd(idx: Int) = (idx and 0x1) == 1
        fun parent(treeIdx: Int) = treeIdx / 2
        fun leftChild(treeIdx: Int) = treeIdx * 2
        fun rightChild(treeIdx: Int) = treeIdx * 2 + 1
    }

}