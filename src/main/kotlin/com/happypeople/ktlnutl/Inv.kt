package com.happypeople.codeforces

//val defaultMod = 998244353;
val defaultMod = 1000000007

/** TODO Test */
class Inv {
    companion object {
        val defaultMod = 1000000007
        var MOD = defaultMod

        fun toPower(a: Int, p: Int, mod: Int=MOD): Int {
            var a = a
            var p = p
            var res = 1
            while (p != 0) {
                if (p and 1 == 1)
                    res = mul(res, a)
                p = p shr 1
                a = mul(a, a)
            }
            return res
        }

        fun inv(x: Int, mod: Int = MOD): Int {
            return toPower(x, mod - 2)
        }

        fun fraction(zaehler: Int, nenner: Int): Int {
            return mul(zaehler, inv(nenner))
        }

        fun mul(v1: Int, v2: Int): Int {
            return ((1L * v1 * v2) % Inv.MOD).toInt()
        }

        fun plus(v1: Int, v2: Int): Int {
            var res = v1 + v2

            if (res < 0)
                res += Inv.MOD

            if(res>=Inv.MOD)
                res-=Inv.MOD

            return res
        }
    }
}