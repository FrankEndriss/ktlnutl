package com.happypeople.ktlnutl

/**  Class for building groups of nodes. Initially every node is its own group.
 * see https://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf
 * By calling merge(x, y) one can merge two groups.
 * By calling getLeader(x) one can query the group of a node.
 * @param n size of structure, max value+1 for x, y */
class UnionFind(val n: Int) {
    val p = IntArray(n) { it }

    /** @return the leader for node pX, which is initially pX. */
    fun getLeader(pX: Int): Int {
        var x = pX
        while (x != p[x]) {
            p[x] = p[p[x]]
            x = p[x]
        }
        return x
    }

    /**  Merges all nodes of the group of pX into the group of pY. After this call all nodes of both groups
     * will have the same leader.
     * @param pX first node
     * @param pY second node
     */
    fun merge(pX: Int, pY: Int) {
        val x = getLeader(pX)
        val y = getLeader(pY)
        p[x] = y
    }

    /** @return an IntArray with all the sizes of the groups set at the leader position.
     * ie sizes()[i] is the size of the group with leader i. */
    fun sizes(): IntArray {
        val ret = IntArray(p.size)
        for (i in p.indices)
            ret[getLeader(i)]++
        return ret
    }

    /** @return an Array of the groups, each one at the leaders index. */
    fun groups(): Array<MutableList<Int>> {
        val ret = arrOf(n) { mutableListOf<Int>() }
        for (i in p.indices)
            ret[getLeader(i)].add(i)

        return ret
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }
}
