package com.happypeople.ktlnutl

class ArrayInit {
    // example of 3-dimensional array of Long
    val NMAX=200
    val dp = arrOf(NMAX) { arrOf(NMAX) { LongArray(NMAX) }}

    /** Method usefull to initialize an array of generic type,
     * especially arrays of arrays.
     */
    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }
}