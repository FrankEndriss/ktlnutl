package com.happypeople.codeforces

/**
 * see https://stackoverflow.com/questions/11208446/generating-permutations-of-a-set-most-efficiently
 */
class Permutations {
    companion object {
        /** @return Sequence of all permutations of List<T>, including List<T> itself.
         * The order is so that it "counts up",
         * ie [1,2,3] -> [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]
         */
        fun <T> permutations(col: Iterable<T>): Sequence<List<T>> {
            val list = col.toList()
            var idx:IntArray? = null

            return generateSequence {
                if (idx==null) {
                    idx = (0..list.size - 1).toList().toIntArray()
                    list
                } else {
                    if (nextPermutation(idx!!))
                        idx!!.map { list[it] }
                    else
                        null
                }
            }
        }

        fun nextPermutation(numList: IntArray): Boolean {
            /*
            by Sani Singh Huttunen
         Knuths
         1. Find the largest index j such that a[j] < a[j + 1]. If no such index exists, the permutation is the last permutation.
         2. Find the largest index l such that a[j] < a[l]. Since j + 1 is such an index, l is well defined and satisfies j < l.
         3. Swap a[j] with a[l].
         4. Reverse the sequence from a[j + 1] up to and including the final element a[n].
         */
            var largestIndex = -1
            for (i in numList.size - 2 downTo 0) {
                if (numList[i] < numList[i + 1]) {
                    largestIndex = i
                    break
                }
            }

            if (largestIndex < 0)
                return false

            var largestIndex2 = -1
            for (i in numList.size - 1 downTo 0) {
                if (numList[largestIndex] < numList[i]) {
                    largestIndex2 = i
                    break
                }
            }

            var tmp = numList[largestIndex]
            numList[largestIndex] = numList[largestIndex2]
            numList[largestIndex2] = tmp

            var i = largestIndex + 1
            var j = numList.size - 1
            while (i < j) {
                tmp = numList[i]
                numList[i] = numList[j]
                numList[j] = tmp
                i++
                j--
            }

            return true
        }
    }
}