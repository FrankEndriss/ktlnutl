package com.happypeople.codeforces

// see https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
// The Bron–Kerbosch algorithm is an algorithm for finding maximal cliques in an undirected graph.
// TODO change implementation to static methods and graph as Set<Pair<N, N>>
// TODO add tests
class BronKerboschPivot<T>(val graph: Map<T, Set<T>>) {

    fun n(t: T): Set<T> {
        return graph[t]!!
    }

    /** Analyze the graph
     * @return set of biggest groups of connected nodes in the graph, size of all these groups is equal.
     * So, most likely it is only one group.
     **/
    fun run(): Set<Set<T>> {
        val results = mutableSetOf<Set<T>>()
        execute(setOf<T>(), graph.keys.toSet(), setOf<T>(), results, 1)
        return results
    }

    private fun reportResult(results: MutableSet<Set<T>>, r: Set<T>) {
        if (results.isEmpty())
            results.add(r)
        else if (results.first().size < r.size) {
            results.clear()
            results.add(r)
        } else if (results.first().size == r.size)
            results.add(r)
    }

    private fun execute(r: Set<T>, p: Set<T>, x: Set<T>, results: MutableSet<Set<T>>, depth: Int) {
        // println("execute, sizes: ${r.size} ${p.size} ${x.size}, depth=$depth")
        if (p.isEmpty() && x.isEmpty()) {
            reportResult(results, r)
            // println("results.size: ${results.size}, depth=$depth")
        } else {
            val pivot = p.plus(x).maxBy { n(it).size }!!
            // val pivot = p.plus(x).first()
            p.minus(n(pivot)).forEach { v ->
                execute(r.plus(v), p.intersect(n(v)), x.intersect(n(v)), results, depth + 1)
            }
        }
    }
}
