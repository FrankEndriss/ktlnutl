package com.happypeople.codeforces

/** Finds the minimum weighted spanning tree in a graph.
 * see https://de.wikipedia.org/wiki/Algorithmus_von_Kruskal
 * TODO add tests, see c1095/F.kt for tests
 */
class Kruskal {
    interface Graph<W> {
        /** Must call cb for all edges in the graph, edges sorted by w, stop loop if cb returns false. */
        fun forEachEdge(cb: (w: W, x: Int, y: Int) -> Boolean)
    }

    companion object {

        /** Neat mechanism to group added edges by subgraph.
         * see https://de.wikipedia.org/wiki/Union-Find-Struktur */
        private fun merge(p: IntArray, pX: Int, pY: Int): Boolean {
            var x = getLeader(p, pX)
            var y = getLeader(p, pY)
            if (x == y)
                return false
            p[x] = y
            return true
        }

        private fun getLeader(p: IntArray, pX: Int): Int {
            if (pX == p[pX])
                return pX
            p[pX] = getLeader(p, p[pX])
            return p[pX]
        }

        /** Constructs a mimimum spanning tree, calls result for every added edge. Stops
         * if no more edges or maxNodes is reached. */
        public fun <W> minimumSpanningTree(graph: Graph<W>, maxNodes: Int, result: (w: W, x: Int, y: Int) -> Unit) {

            val p = IntArray(maxNodes) { it }
            //for (i in p.indices)
            //    p[i] = i

            var nodeCount = 0

            graph.forEachEdge { w, x, y ->
                if (merge(p, x, y)) {
                    nodeCount++
                    result(w, x, y)
                }
                nodeCount < maxNodes
            }
        }
    }
}
