package com.happypeople.ktlnutl

/* Generating primes. */
class Primes(val MAXN:Int, val primeAproxFactor:Int=30) {
    val pr = IntArray(MAXN)

    init {
        val cmp = BooleanArray(MAXN * primeAproxFactor)

        var c = 0
        var i = 2
        // approx pr[MAXN] to be less than MAXN*primeAproxFactor, which is true for about MAXN < ~200000
        while (i < MAXN * primeAproxFactor && c < MAXN) {
            if (!cmp[i]) {
                pr[c] = i
                c++
                var j = 1L*i * i
                while (j < primeAproxFactor * MAXN) {
                    cmp[j.toInt()] = true
                    j += i
                }
            }
            i++
        }
    }

    fun asSeq(): Sequence<Int> {
        var idx = 0
        return generateSequence {
            if (idx >= pr.size)
                null
            else
                pr[idx++]
        }
    }

    /*
const int MAXN = 100000;
int pr[MAXN], c = 0;
bool cmp[MAXN * 30];

void precalc() {
c = 0;
memset(cmp, false, sizeof cmp);
for (int i = 2; i< 30 * MAXN && c < MAXN; ++i) {
    if (!cmp[i]) {
        pr[c++] = i;
        for (long long j = i*1ll*i; j < 30 * MAXN; j += i) {
            cmp[j] = true;
        }
    }
}
}
     */
}