package com.happypeople.ktlnutl

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KtlnutlApplication

fun main(args: Array<String>) {
	runApplication<KtlnutlApplication>(*args)
}

