package com.happypeople.codeforces

class Lsb { companion object { fun lsb(i: Int) = i and -i } }
/* BinaryIndexTree see
https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/
https://en.wikipedia.org/wiki/Fenwick_tree
https://www.youtube.com/watch?v=v_wj_mOAlig
https://github.com/jakobkogler/Algorithm-DataStructures/blob/master/RangeQuery/BinaryIndexedTree.cpp
https://codeforces.com/problemset/problem/1093/E
 */
/** Two dimensional Fenwick, uses less memory if sparse==true. */
class Fenwick2D<E>(val sizeX: Int, val sizeY: Int, val neutral: E, val sparse: Boolean = false, val cumul: (E, E) -> E) {
    val tree= arrayOfNulls<FenwickIf<E>>(sizeX+1)

    fun treeget(x:Int):FenwickIf<E> {
        if(tree[x]==null) {
            tree[x] = if (sparse)
                FenwickSparse<E>(sizeY, neutral, cumul)
            else
                Fenwick<E>(sizeY, neutral, cumul)
        }
        return tree[x]!!
    }

    /** Updates the value at pIx/pIy cumulative, i.e. "adds" value to the value stored at pIx/pIy. */
    fun updateCumul(pIx: Int, pIy: Int, value: E) {
        if (pIx < 1 || pIx>tree.size)
            throw IllegalArgumentException("bad idx1, min=1, max=${tree.size}, you sent: $pIx")
        var x = pIx
        while (x < tree.size) {
            treeget(x).updateCumul(pIy, value)
            x += Lsb.lsb(x)
        }
    }

    /** @return the cumul of range (0,0):(pIx, pIy) inclusive. */
    fun readCumul(pIx: Int, pIy: Int): E {
        var x = pIx
        var sum = neutral
        while (x > 0) {
            sum = cumul(sum, treeget(x).readCumul(pIy))
            x -= Lsb.lsb(x)
        }
        return sum
    }
}

/** Sparse Fenwick tree implementation, uses a map instead of an array.
 * Usefull if less nodes are used.
 * @param E Type of elements
 * @param neutral The neutral element according to cumul.
 * @param cumul The cumulative function, ie sum() or the like.
 **/
class FenwickSparse<E>(val sizeofTree: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
    val tree = mutableMapOf<Int, E>()
    /** TODO find a way to dynamically grow the tree to new sizes. */

    /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx. */
    override fun updateCumul(pIdx: Int, value: E) {
        if (pIdx < 1 || pIdx>sizeofTree)
            throw IllegalArgumentException("bad idx1, min=1, max=${sizeofTree}, you sent: $pIdx")
        var idx = pIdx
        while (idx < sizeofTree) {
            tree[idx] = cumul(tree.getOrDefault(idx, neutral), value)
            idx += Lsb.lsb(idx)
        }
    }

    /** @return the cumul of range 0:pIdx inclusive. */
    override fun readCumul(pIdx: Int): E {
        var idx = pIdx
        var sum = neutral
        while (idx > 0) {
            sum = cumul(sum, tree.getOrDefault(idx, neutral))
            idx -= Lsb.lsb(idx)
        }
        return sum
    }

}

// TODO FenwickInt, based on IntArray
// TODO Fenwick2DInt, based on single IntArray
// TODO Fenwick2DSparse, both dimension based on Map

/** All purpose Fenwick tree implementation. Note that Idx is 1 based, idx=0 must not be used.
 * @param E Type of elements
 * @param size Max index into tree.
 * @param neutral The neutral element according to cumul.
 * @param cumul The cumulative function, ie sum() or the like.
 **/
class Fenwick<E>(size: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
    val tree = (0..size).map { neutral }.toMutableList()

    override fun updateCumul(pIdx: Int, value: E) {
        if (pIdx < 1 || pIdx>tree.size)
            throw IllegalArgumentException("bad idx1, min=1, max=${tree.size}, you sent: $pIdx")
        var idx = pIdx
        while (idx < tree.size) {
            tree[idx] = cumul(tree[idx], value)
            idx += Lsb.lsb(idx)
        }
    }

    /** Read a single value at  pIdx, needs an uncumul function, ie minus if used plus for cumul. */
    fun readSingle(pIdx: Int, uncumul: (E, E) -> E): E {
        var idx = pIdx
        var sum = tree[idx]
        if (idx > 0) {
            val z = idx - Lsb.lsb(idx)
            idx--
            while (idx != z) {
                sum = uncumul(sum, tree[idx])
                idx -= Lsb.lsb(idx)
            }
        }
        return sum
    }

    override fun readCumul(pIdx: Int): E {
        var idx = pIdx
        var sum = neutral
        while (idx > 0) {
            sum = cumul(sum, tree[idx])
            idx -= Lsb.lsb(idx)
        }
        return sum
    }
}

interface FenwickIf<E> {
    /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx.
     * @param pIdx 1 based index into structure.
     * @param value the delta for the Update. */
    fun updateCumul(pIdx: Int, value: E)

    /** @return the cumul of range 0:pIdx inclusive. */
    fun readCumul(pIdx: Int): E

}

