mod x {
    /*
    use std::cmp::min;
    use std::cmp::max;
    */

   /* Pattern for implmenting orderable class requires a working PartialOrd _AND_ Ord, not the derived ones!
	But we can derive Eq and PartialEq


    #[derive(Debug,Eq,PartialEq)]
    struct X {
        s: String
    }

    impl Ord for X {
        fn cmp(&self, other: &Self) -> Ordering {
            self.s.as_bytes()[1].cmp(&other.s.as_bytes()[1])    // compare char at pos==1 as example
        }
    }

    impl PartialOrd for X {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }
*/

    /**
     */
    pub fn solve(input: &String) {
/* parsing examples:
let parts:Vec<_>=input.split("\n\n").into_iter().collect();
let (left, right) = line.split_once(":").unwrap();
let a = right.split_ascii_whitespace()
                    .map(|s| s.parse::<u64>().unwrap())
                    .collect();
*/
        let mut inp=input.split_ascii_whitespace();
        let n: usize = inp.next().unwrap().parse().unwrap();

        println!("{n}");
    }
}

use std::io::{self,Read};

fn main() {
    let mut buf=String::new();
    io::stdin().read_to_string(&mut buf).unwrap();
    x::solve(&buf);
}

#[cfg(test)]
mod test {
    use test_toolbox::capture;
    use crate::x;

    #[test]
    fn my_test() {
        let input = "
            42
            ".to_string();
        let (out, _) = capture! {{
            x::solve(&input)
        }};
        assert_eq!("42\n", out);
    }
}
