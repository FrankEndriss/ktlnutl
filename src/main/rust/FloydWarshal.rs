template<typename T>
pub fn floyd_warshal(d: &mut Vec<Vec<T>>, n:usize) {
    for k in 0..n {
        for i in 0..n {
            for j in 0..n {
                d[i][j] = d[i][j].min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

/* sets all d[i][j]==-INF which are part of a negative cycle after floyd_warshal(d) 
 * see https://cp-algorithms.com/graph/finding-negative-cycle-in-graph.html
template<typename T>
void check_negativ_cycles(vector<vector<T>> &d, int n, T INF) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int t = 0; t < n; t++) {
                if (d[i][t] < INF && d[t][t] < 0 && d[t][j] < INF)
                    d[i][j] = - INF;
            }
        }
    }
}
 * */
