/* some context for calculation */
struct Ctx {
    x:f64,
    y:f64
}

impl Ctx {
    fn new(x:f64, y:f64) -> Self {
        Ctx {x,y}
    }   

    fn calc(&self, arg:f64) -> f64 {
        // TODO calc some value for arg
    }
}

/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f.calc(x) is max */
fn terns(l: f64, r: f64, f: &Ctx) -> f64 {
    let mut l=l;
    let mut r=r;
    for _ in 0..70 {
        let l3 = l + (r - l) / 3.0;
        let r3 = r - (r - l) / 3.0;
        if f.calc(l3) < f.calc(r3) {
            l = l3;
        } else {
            r = r3;
        }
    }
    (l + r) / 2.0
}
