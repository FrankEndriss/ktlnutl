/*
see https://cp-algorithms.com/string/prefix-function.html
*/
          
fn compute_kmp_table(pattern: &Vec<usize>) -> Vec<isize> {
    let mut table = vec![-1; pattern.len()];
    let mut j: isize = -1;
    for i in 1..pattern.len() {
        while j >= 0 && pattern[j as usize] != pattern[i - 1] {
            j = table[j as usize];
        }
        j += 1;
        if pattern[i] == pattern[j as usize] {
            table[i] = table[j as usize];
        } else {
            table[i] = j;
        }
    }
    table
}

fn kmp_search(haystack: &Vec<usize>, needle: &Vec<usize>) -> Option<usize> {
    if needle.is_empty() {
        return Some(0);
    }

    let table = compute_kmp_table(needle);
    let mut m = 0;  // beginning of the current match in haystack
    let mut i = 0;  // position of the current character in needle

    while m + i < haystack.len() {
        if needle[i] == haystack[m + i] {
            if i == needle.len() - 1 {
                return Some(m);
            }
            i += 1;
        } else {
            if table[i] > -1 {
                m = m + i - table[i] as usize;
                i = table[i] as usize;
            } else {
                m += 1;
                i = 0;
            }
        }
    }
    None
}

