pub struct Names { snames: Vec<String>, names: HashMap<String,usize> }

impl Names { 
    pub fn new(n: usize) -> Self { 
        Self { 
            snames: vec!["".to_string(); n], 
            names: HashMap::<String,usize>::new()
        }
    }
    fn toi(&mut self, name:&String) -> usize {
        let mut
        v=self.names.len(); 
        match self.names.entry(name.to_string()) {
            Entry::Vacant(_ent) => {
                self.names.insert(name.to_string(), v); 
                self.snames[v]=name.to_string();
            }
            Entry::Occupied(ent) => {
                v=*ent.get();
            }
        };
        v
    }
    fn tos(&self, i:usize) -> &str {
        self.snames[i].as_str()
    }
}
