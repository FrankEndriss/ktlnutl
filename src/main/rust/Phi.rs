pub fn phi(n:usize, pr:Vec<usize>) -> usize {
    
    // Initialize result as n
    let mut result = n as f64; 
 
    // Consider all prime factors of n 
    // and for every prime factor p,
    // multiply result with (1 - 1/p)

    for p in pr.iter() {
        if p*p>n {
            result -= result / *p as f64;
        } else {
            result *= 1.0 - (1.0 / *p as f64);
        }
    }
 
    (result+0.1f64).floor() as usize
}