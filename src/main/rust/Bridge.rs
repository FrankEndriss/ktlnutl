// see https://cp-algorithms.com/graph/bridge-searching.html
// find all bridges of a graph in O(n+m)

struct Bfinder {
        visited: Vec<bool>,
        tin: Vec<usize>,
        low: Vec<usize>,
        timer: usize,
        ans: Vec<(usize, usize)>,
    }
    impl Bfinder {
        fn new(n:usize) -> Self {
            Bfinder {
                visited: vec![false; n],
                tin: vec![0; n],
                low: vec![0; n],
                timer: 0,
                ans: vec![(0usize, 0usize); 0],
            }
        }
        fn dfs(&mut self, adj:&Vec<Vec<usize>>, v: usize, p: usize) {
            self.visited[v] = true;
            self.low[v] = self.timer;
            self.tin[v] = self.timer;
            self.timer += 1;
            let toidx: Vec<_> = adj[v].clone();
            for to in toidx.iter() {
                if *to == p {
                    continue;
                }
                if self.visited[*to] {
                    self.low[v] = self.low[v].min(self.tin[*to]);
                } else {
                    self.dfs(adj, *to, v);
                    self.low[v] = self.low[v].min(self.low[*to]);
                    if self.low[*to] > self.tin[v] {
                        self.ans.push((v, *to));
                        //IS_BRIDGE(v, to);
                    }
                }
            }
        }

        fn find_bridges(&mut self, adj:&Vec<Vec<usize>>) {
            self.timer = 0;
            //visited.assign(n, false);
            //tin.assign(n, -1);
            //low.assign(n, -1);
            for i in 0..adj.len() {
                if !self.visited[i] {
                    self.dfs(adj, i, usize::MAX);
                }
            }
        }
    }
