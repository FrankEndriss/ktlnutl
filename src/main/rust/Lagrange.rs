
/**https://projecteuler.net/problem=101
 * see 
 * Prob asks basically to find the "minimal" polynomial that generates a certain sequence.
 * There is a method called lagrange polynominal, see
 * https://www.youtube.com/watch?v=WCGKqJrf4N4
 * https://en.wikipedia.org/wiki/Lagrange_polynomial
 */
fn gcd(a: i128, b: i128) -> i128 {
    let mut a = a.abs();
    let mut b = b.abs();

    while b != 0 {
        let temp = b;
        b = a % b;
        a = temp;
    }

    a
}

/* We want to interpolate a value f(xk) for a unknown function f. 
 * But we have certain data points y[] at positions x[]
 * This method creates the so called optimum polynomial generating function,
 * and used it to calculate the value at position xk.
 * 
 * It also works for floats, then use without gcd, instead simply devide.
 */
pub fn lagrange_polynomial(x:&Vec<i128>, y:&Vec<i128>, xk:i128) -> i128 {
    assert!(x.len()==y.len());
    let n=y.len();

    let mut nom=0i128;
    let mut den=1i128;
    for i in 0..n {
        let mut nom0=y[i]*(0..n).map(|j| if i==j { 1 } else { xk-x[j]}).fold(1i128, |p,k| { p*k});
        let den0=(0..n).map(|j| if i==j { 1 } else { x[i]-x[j]}).fold(1i128, |p,k| { p*k});

        nom0*=den;
        nom*=den0;
        den*=den0;

        let g=gcd(nom0+nom, den);
        nom=(nom+nom0)/g;
        den/=g;
    }

    nom/den
}
