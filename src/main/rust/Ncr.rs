
    struct Ncr {
        fac: Vec<ModInt1000000007>
    }
    
    impl Ncr {
        fn new(n:usize) -> Self {
            let mut fac = vec![ModInt1000000007::new(0);n+1];
            fac[0]=ModInt1000000007::new(1);
            for i in 1..n+1 {
                fac[i]=fac[i-1]*i;
            }
            Ncr { fac }
        }
    
        fn ncr(&self, n:usize, k:usize) -> ModInt1000000007 {
            if k==0 || k==n {
                ModInt1000000007::new(1)
            } else if k==1 || k==n-1 {
                ModInt1000000007::new(n)
            } else if k>n/2 {
                self.ncr(n, n-k)
            } else {
                self.fac[n]*self.fac[k].inv()*self.fac[n-k].inv()
            }
        }
    }