 vsc plugin mathover
 https://www.overleaf.com/learn/latex/Mathematical_expressions

 # Math: \sqrt2=1+\frac{1}{2+\frac{1}{2+{\frac{1}{2+\cdot}}}}
 # Math: \binom{n}{k} = \frac{n!}{k!(n-k)!}
 # Math: x=p_1^{a_1} p_2^{a_2} \cdots p_n^{a_n} = \prod_{i=1}^\infty{p_i^{a_i}}
 # Math: \sum_{n=1}^{\infty} 2^{-n} = 1
 # Math: \int_{a}^{b} x^2 \,dx
 # Math: \lim_{x\to\infty} f(x)
 # Math: < > \subset \supset \subseteq \supseteq
 # Math: \times \otimes \oplus \cup \cap
 # Math: \alpha \beta \gamma \rho \sigma \delta \epsilon
 # Math: A B \Gamma P \Sigma \Delta E \Theta
 # Math: \sqrt{x^2+1}
 # Math: E=mc^2
 # Math: x^n + y^n = z^n 