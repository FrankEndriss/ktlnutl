pub fn primes(n: usize) -> (Vec<bool>,Vec<usize>) {

    let mut notpr=vec![false;n];
    let mut pr=vec![0usize;0];

    for i in 2..n {
        if !notpr[i] {
            pr.push(i);
            for j in (i+i..n).step_by(i) {
                notpr[j]=true;
            }
        }
    }
    (notpr,pr)
}

/// Performs modular exponentiation: (base^exp) % modulus
fn modular_exponentiation(mut base: u128, mut exp: u128, modulus: u128) -> u128 {
    let mut result = 1_u128;
    base %= modulus;

    while exp > 0 {
        if exp % 2 == 1 {
            result = (result * base) % modulus;
        }
        base = (base * base) % modulus;
        exp /= 2;
    }

    result
}

/// Checks if `n` is a prime number using the Miller-Rabin primality test.
/// This is a probabilistic test, but accurate for the specified range with these bases.
/// `n` must be greater than 1.
fn is_prime(n: u128) -> bool {
    // Handle small numbers directly
    if n <= 1 {
        return false;
    }
    if n <= 3 {
        return true;
    }
    if n % 2 == 0 {
        return false;
    }

    // Decompose (n - 1) = d * 2^s
    let mut d = n - 1;
    let mut s = 0;
    while d % 2 == 0 {
        d /= 2;
        s += 1;
    }

    // Miller-Rabin bases for testing (deterministic for n < 2^64)
    let bases: [u128; 7] = [2, 3, 5, 7, 11, 13, 17];

    // Test the bases
    for &a in &bases {
        if a >= n {
            continue;
        }
        let mut x = modular_exponentiation(a, d, n);
        if x == 1 || x == n - 1 {
            continue;
        }

        let mut is_composite = true;
        for _ in 0..(s - 1) {
            x = modular_exponentiation(x, 2, n);
            if x == n - 1 {
                is_composite = false;
                break;
            }
        }

        if is_composite {
            return false;
        }
    }

    true
}
