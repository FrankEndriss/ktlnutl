/* see https://cp-algorithms.com/data_structures/sqrt_decomposition.html#toc-tgt-4
 * Queries are inclusive l and inclusive r.
 **/

//const int BLOCK_SIZE=(int)sqrt(5e5+5)+1;
const BLOCK_SIZE:usize=512;

/* special sort function for Query */
pub fn mo_qrycmp(p:&Query, q:&Query) -> std::cmp::Ordering {
    if (p.0 / BLOCK_SIZE != q.0 / BLOCK_SIZE) {
        return (p.l,p.r).cmp((q.l.q.r));
    }

    if (p.l/BLOCK_SIZE)%2==1 {
        p.r.cmp(q.r)
    } else {
        match p.r.cmp(q.r) {
            Ordering::Equal => Ordering::Equal,
            Ordering::Less => Ordering::Greater,
            Ordering::Greater => Ordering::Less 
        }
    }
}

trait Mo {
    fn remove(idx:usize);
    fn add(idx:usize);
    fn get_answer() -> i64;
}

/* (l,r) both inclusive, 0 based */
struct Query {
    l:usize,
    r:usize,
    idx:usize
}

/* example impl of the three functions
 ** for finding number of numbers in
 ** interval where freq==number 
 */
 struct MyMo {
    gans:usize,
    f:Vec<usize>,
    a:Vec<usize>
 }


impl Mo for MyMo {
    fn new(a:Vec<usize>, n:usize) -> MyMo {
        MyMo {
            gans: 0,
            f:vec![0usize;n],
            a
        }
    }

    fn remove(&self, idx:usize) {
        f[a[idx]]-=1;
        if f[a[idx]]==a[idx] {
            gans+=1;
        } else if f[a[idx]]+1==a[idx] {
            gans-=1;
        }
    }

    fn add(&self, idx:usize) {
        f[a[idx]]+=1;
        if f[a[idx]]==a[idx] {
            gans+=1;
        } else if f[a[idx]]==a[idx]+1 {
            gans-=1;
        }
    }

    fn get_answer(&self) {
        return gans;
    }
}


pub fn mo_run(queries:&mut Vec<Query>, mo:&Mo) -> Vec<i64> {
    let mut answers:vec![0i64;queries.size()];
    queries.sort_by(mo_qrycmp);
    let mut cur_l = 0usize;
    let mut cur_r = -1i64;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
    for &q in queries.iter() {
        while cur_l > q.l {
            cur_l-=1;
            mo.add(cur_l);
        }
        while cur_r < q.r {
            cur_r+=1;
            mo.add(cur_r);
        }
        while cur_l < q.l {
            mo.remove(cur_l);
            cur_l+=1;
        }
        while cur_r > q.r {
            mo.remove(cur_r);
            cur_r-=1;
        }
        answers[q.idx] = mo.get_answer();
    }
    return answers;
}
