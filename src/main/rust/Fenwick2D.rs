struct Fenwick2D {
    a: Vec<Vec<i64>>
}

impl Fenwick2D {
    pub fn new(n: usize) -> Self {
        Fenwick2D {
            a: vec![vec![0i64;n];n]
        }
    }

    /* Query sum excluding (x,y) */
    pub fn sum(&self, x:usize, y:usize) -> i64 {
        if x>0 && y>0 {
            self.sum_inc(x-1,y-1)
        } else {
            0
        }
    }

    /* Query sum including (x,y) */
    fn sum_inc(&self, x:usize, y:usize) -> i64 {
        let mut ret = 0;
        let mut i=x as i64;
        while i >= 0 {
            let mut j=y as i64;
            while j>=0 {
                ret += self.a[i as usize][j as usize];
                j = (j & (j + 1)) - 1;
            }
            i=(i & (i + 1)) - 1;
        }
        ret
    }

    /* add at (x,y) */
    pub fn add(&mut self, x:usize, y:usize, delta:i64) {
        let mut i=x;
        while i<self.a.len() {
            let mut j=y;
            while j<self.a[i].len() {
                self.a[i][j] += delta;
                j = j | (j + 1);
            }
            i= i | (i + 1)
        }
    }
}