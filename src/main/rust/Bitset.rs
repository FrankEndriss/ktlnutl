/* Bitset impl in Rust */
#[derive(Clone, Eq, Hash, PartialEq, PartialOrd, Ord)]
struct BitSet {
    data: Vec<u64>,
    bitset_size: usize
}

impl BitSet {
    const BLOCK_SIZE: usize = 64;

    fn new(bitset_size:usize) -> Self {
        let blocks = (bitset_size + Self::BLOCK_SIZE - 1) / Self::BLOCK_SIZE;
        let data = vec![0; blocks];
        BitSet { data,bitset_size }
    }

    fn set(&mut self, pos: usize, value: bool) {
        if pos < self.bitset_size {
            let block_index = pos / Self::BLOCK_SIZE;
            let bit_index = pos % Self::BLOCK_SIZE;

            if value {
                self.data[block_index] |= 1 << bit_index;
            } else {
                self.data[block_index] &= !(1 << bit_index);
            }
        }
    }

    fn get(&self, pos: usize) -> bool {
        if pos < self.bitset_size {
            let block_index = pos / Self::BLOCK_SIZE;
            let bit_index = pos % Self::BLOCK_SIZE;
            (self.data[block_index] >> bit_index) & 1 != 0
        } else {
            false
        }
    }

    fn and(&self, other: &BitSet) -> BitSet {
        let mut result = BitSet::new(self.bitset_size.min(other.bitset_size));
        for i in 0..self.data.len().min(other.data.len()) {
            result.data[i] = self.data[i] & other.data[i];
        }
        result
    }

    fn or(&self, other: &BitSet) -> BitSet {
        let mut result = BitSet::new(self.bitset_size.min(other.bitset_size));
        for i in 0..self.data.len().min(other.data.len()) {
            result.data[i] = self.data[i] | other.data[i];
        }
        result
    }

    fn xor(&self, other: &BitSet) -> BitSet {
        let mut result = BitSet::new(self.bitset_size.min(other.bitset_size));
        for i in 0..self.data.len().min(other.data.len()) {
            result.data[i] = self.data[i] ^ other.data[i];
        }
        result
    }

    fn not(&self) -> BitSet {
        let mut result = BitSet::new(self.bitset_size);
        for i in 0..self.data.len() {
            result.data[i] = !self.data[i];
        }
        result
    }

    /* finds next unset index in bs right of i */
    fn nextunset(&self, i: usize) -> usize {
        let mut ans: usize = i + 1;
        loop {
            if !self.get(ans) {
                break;
            } else {
                ans += 1;
            }
        }
        ans
    }

    fn count_ones(&self) -> usize {
        let mut ans=0usize;
        for i in &self.data {
            ans+=i.count_ones() as usize;
        }
        ans
    }
}