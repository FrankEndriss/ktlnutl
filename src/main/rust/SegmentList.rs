
/*  s holds a list of non-overlapping segments [l,r)
    Incuding l, excluding r
    This calls adds a segment to s.
@return number of positions actually covered by this call that where not covered before.
*/
pub fn add_segment(s:&mut BTreeSet::<(usize,usize)>, l:usize, r:usize) -> usize {
    let mut rm=Vec::<(usize,usize)>::new();

    // note that we allways insert one segment, and maybe remove some
    let mut ll=l;
    let mut rr=r;
    // find all with existing.r>=new.l
    for &(r0,l0) in s.range((l,0)..) {
        if l0>r {
            break;
        } else {
            ll=ll.min(l0);
            rr=rr.max(r0);
        }
        rm.push((r0,l0));
    }
    let mut ans=rr-ll;
    for &(r,l) in rm.iter() {
        ans-=(r-l);
        s.remove(&(r,l));
    }
    s.insert((rr,ll));
    ans
}