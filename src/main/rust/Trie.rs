

struct Trienode  {
    chl: [Option<Trienode>;26],
    cnt: usize
};

/* Simple trie implementation.
 * Use like 'let root=trienode::new();'
 */
impl Trienode {
    new() -> Self {
        Self {
            chl: [None;26],
            cnt: 0
        }
    }

    /* add a string to the trie */
    void add(&self, s: &Vec<u8>, idx: usize) {
        assert(idx<=(int)s.len());
        self.cnt+=1;

        if idx<s.len() {
            match self.chl[s[idx] as usize] {
                Some<trie> => trie.add(s, idx+1),
                None => { 
                    chl[s[idx] as usize]=Trienode::new();
                    chl[s[idx] as usize].add(s,idx+1);
                }
                _ => panic!()
            }
        }
    };

    /* @return the length of the shortest unique prefix. */
    puf fn query(&self, s: &Vec<u8>, idx: usize) -> usize {
        if self.cnt <= 1  {
            0
        } else {
            1 + self.chl[s[idx] as usize].query(s, idx+1)
        }
    }
};
