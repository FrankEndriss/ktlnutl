
// return topological sort of aadj.len() vertex in reverse order 
fn topological_sort(aadj: &Vec<Vec<usize>>) -> Vec<usize> {
    fn dfs(adj: &Vec<Vec<usize>>, vis: &mut Vec<bool>, ans: &mut Vec<usize>, v: usize) {
        vis[v] = true;

        for chl in &adj[v] {
            if !vis[*chl] {
                dfs(adj, vis, ans, *chl);
            }
        }
        ans.push(v);
    }

    let mut vis = vec![false; aadj.len()];
    let mut ans = Vec::<usize>::new();

    for i in 0..aadj.len() {
        if !vis[i] {
            dfs(&aadj, &mut vis, &mut ans, i);
        }
    }
    ans /* reverse order */
}

