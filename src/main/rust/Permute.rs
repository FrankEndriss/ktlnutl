/* create all permutations of a vector */
fn permute(vec: &mut Vec<(i32,i32)>, start: usize, results: &mut Vec<Vec<(i32,i32)>>) {
    if start == vec.len() - 1 {
        results.push(vec.clone());
        return;
    }

    for i in start..vec.len() {
        vec.swap(start, i);
        permute(vec, start + 1, results);
        vec.swap(start, i); // backtrack
    }
}