pub fn gcd(a:i64, b:i64) -> i64 {
    if b == 0 { a } else { gcd(b, a%b) }
}

#[derive(Clone,Debug)]
struct Frac {
    nom: i64,
    den: i64
}
impl Frac {
    pub fn new(nom: i64, den: i64) -> Self {
        let mut ans=Self { nom, den };
        ans.norm();
        ans
    }

    pub fn norm(&mut self) {
        if self.den<0 {
            self.nom=-self.nom;
            self.den=-self.den;
        }

        if self.nom==0 {
            self.den=1;
        } else {
            let g=gcd(self.nom.abs(),self.den.abs());
            self.nom/=g;
            self.den/=g;
        }
    }

    pub fn mul(&self, other: &Self) -> Self {
        Frac::new(self.nom*other.nom, self.den*other.den)
    }
    pub fn add(&self, other: &Self) -> Self {
        Frac::new(self.nom*other.den+other.nom*self.den, self.den*other.den)
    }
    pub fn sub(&self, other: &Self) -> Self {
        Frac::new(self.nom*other.den-other.nom*self.den, self.den*other.den)
    }
}
