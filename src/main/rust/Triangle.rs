
use std::{
    ops::{Add, Mul, Sub}
};
/* @return area*2 of a trieangle, should work for all numeric types,
    note that result could be negative.
*/
pub fn area2<T>(x1:T, y1:T, x2:T, y2:T, x3:T, y3:T) -> T 
where
T: Add<Output=T> + Mul<Output=T> + Sub<Output=T> + Copy
{
    x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2)
}
