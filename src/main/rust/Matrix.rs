#[derive(Debug, Clone)]
struct Matrix(Vec<Vec<u64>>);

impl Matrix {
    fn multiply(&self, other: &Matrix, m: u64) -> Matrix {
        let sz=self.0.len();
        let mut result = Matrix(vec![vec![0u64; sz]; sz]);

        for i in 0..sz {
            for j in 0..sz {
                for k in 0..sz {
                    result.0[i][j] = (result.0[i][j]+ self.0[i][k] * other.0[k][j]) % m;
                }
            }
        }

        result
    }
    fn power(&self, p: u64, m: u64) -> Matrix {
        let sz=self.0.len();
        let mut res = Matrix(vec![vec![0u64; sz]; sz]);
        for i in 0..sz {
            res.0[i][i]=1;
        }
    
        let mut aa=self.clone();
        let mut pp=p;
        while pp != 0 {
            if pp & 1u64 != 0u64 {
                res = res.multiply(&aa, m);
            }
            pp >>= 1;
            aa = aa.multiply(&aa, m);
        }
        return res;
    }
}
